/**
 * Pattern piece - a piece; standalone.
 **/

import phyusicMatter from "./phyusicmatter.js"
import matterFactory from "./matterfactory.js"
import chordProggies from "./chordproggies.js"
import musicEnvironment from "./musicenvironment.js"
import SampleSet from "./samplesets.js"
import pneumPiece from "./pieces/pneum.js"
import deefliPiece from "./pieces/deefli.js"
import lakaPiece from "./pieces/laka.js"
import hazyJangolPiece from "./pieces/hazyjangol.js"
import testPiece from "./pieces/testpiece.js"

import * as Tone from 'tone'
import * as Nexus from 'nexusui'

import AudioRecorder from 'audio-recorder-polyfill'
import mpegEncoder from 'audio-recorder-polyfill/mpeg-encoder'


AudioRecorder.encoder = mpegEncoder
AudioRecorder.prototype.mimeType = 'audio/mpeg'
window.MediaRecorder = AudioRecorder

var matterWorld = new phyusicMatter();
//var myMatterFactory = new matterFactory(); // is this even needed?
//var samples = new SampleSet();
var keys = ['C','Db','D','Eb','E','F','F#','G','Ab','A','Bb','B'];
//var sampleKeys = Object.keys(samples.sets);

var c = new chordProggies;
var proggies = Object.keys(c.chordProgressions);

const recoptions = {
  mimeType: "video/webm"
}
const recorder = new Tone.Recorder(recoptions);

var musicPaused = true;
var musicOptions = {
  "tempo" : 120,//Nexus.ri(30, 240),
  "proggy" : proggies[Nexus.ri(0, 6)],
  "key": keys[0]
};

var instances = null;
var musicEnviro = new musicEnvironment(musicOptions);

var pieceOptions = {
    "enviro" : musicEnviro,
    "world" : matterWorld,
    "color" : '#1e88dd'
    //"samples": samples
};
var currentPiece = new lakaPiece(pieceOptions);

// Piece select
var pieceSelect = new Nexus.Select('#pieces',{
  'size': [80,30],
  'options': ["Laka", "Deefli", "Pneum", "Hazy Jangol", "Test piece"] //, "monoWigging", "gravity", "revgrav"];
});

pieceSelect.on('change', function(v) {
    
    currentPiece.dispose();
    
    pieceOptions.samples = new SampleSet();
    switch(v.value){
      case "Laka":
        currentPiece = new lakaPiece(pieceOptions);
        break
      case "Deefli":
        currentPiece = new deefliPiece(pieceOptions);
        break
      case "Pneum":
        currentPiece = new pneumPiece(pieceOptions);
        break
      case "Test piece":
        currentPiece = new testPiece(pieceOptions);
        break;
      case "Hazy Jangol":
        currentPiece = new hazyJangolPiece(pieceOptions);
        break;

            
    }
    //musicEnviro.start();
    Tone.Transport.position = 0;
});

var toggle = new Nexus.TextButton('#tonetoggle',{
    'size': [80,40],
    'state': false,
    'text': 'Play',
    'alternateText': 'Pause'
})

toggle.on('change', function(v) {
    if (v === true) {
        musicEnviro.start();
    }
    if (v === false) {
        musicEnviro.pauseTone();
    }
});

var recordtoggle = new Nexus.TextButton('#record',{
    'size': [80,40],
    'state': false,
    'text': 'Record',
    'alternateText': 'Stop'
})

recordtoggle.on('change', function(v) {
    if (v === true) {
        console.log("Going to record");
        Tone.Destination.connect(recorder);
        // start recording
        recorder.start();
        
    }
    if (v === false) {
        console.log("Stop recording");
        //const recording = recorder.stop();
        async function stopRecording() {
          
            // download the recording by creating an anchor element and blob url
            const recording = await recorder.stop();
            const url = URL.createObjectURL(recording);
            const anchor = document.createElement("a");
            anchor.download = "phyusic.mp3";
            anchor.href = url;
            anchor.click();
        }
          
        stopRecording();
        
    }
});

var tempo = new Nexus.Number('#tempo',{
  'size': [90,30],
  'value': musicOptions.tempo,
  'min': 30,
  'max': 240,
  'step': 1
});

tempo.on('change', function(v) {
    //console.log(v);
    Tone.Transport.bpm.value = v;
});

var keySelect = new Nexus.Select('#key',{
  'size': [60,30],
  'options': keys
});

keySelect.on('change', function(v) {
  musicEnviro.tonalEnv.setKey(v.index);
  // Update our parts
});

var chordProgSelect = new Nexus.Select('#proggies',{
    'size': [60,30],
    'options': proggies
  });
  
chordProgSelect.on('change', function(v) {
  //musicEnviro.tonalEnv.setKey(v.index);
  musicEnviro.chordProgGen.changeProgression(v.value);
  // Update our parts
});

var resetbutton = new Nexus.Button('#reset',{
  'size': [40,40],
  'mode': 'impulse',
  'state': false
});

resetbutton.on('change',function(v) {
  // v is the value of the button
  console.log(v);
  Tone.Transport.position = 0;
  var txtBox = document.getElementById("position");
  txtBox.value = Tone.Transport.position;
});

const loop = new Tone.Loop((time) => {
// triggered every eighth note.
  var txtBox = document.getElementById("position");
  txtBox.value = Tone.Transport.position;
//console.log(Tone.Transport.position);
}, "4n").start(0);

  









