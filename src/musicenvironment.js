/** @class
* A musicEnvironment provides music for the bolobol game
* @param (Phaser.Game) - game object using this environment
* @param {object} options - json object of options (currently not used)
**/

import Tonality from "./tonality";
import ChordProgGenerator from "./chord_prog_generators/basic_chord_progs";

import * as Tone from 'tone'

function musicEnvironment(options) {

    this.chordProgression = [];
    this.init(options);
}

/**
 * Initialize an environment, with a chord progression,
 * set of samples, and Tone js transport and audio connections.
 * @returns {} -
 **/
musicEnvironment.prototype.init = function(options) {
    this.tonalEnv = new Tonality(options);
  //  this.chordProggies = new chordProggies();
  //  this.chordProgression = this.chordProggies.chordProgressions["1"];
  
    
  
    //this.samples = new SampleSet();

    this.chordProgGen = new ChordProgGenerator(options.proggy);

  //  var randomSwing = game.rnd.realInRange(0,1);
  //Tone.Transport.swing = 0.5;

    Tone.Transport.bpm.value = 112;
    Tone.Transport.loopStart = 0;
    //Tone.Transport.loopEnd = "24m";
    //Tone.Transport.loop = true;
  //  console.log("Tempo is " + Tone.Transport.bpm.value);
    Tone.Transport.latencyHint = "playback";

    this.lowestOctave = 2;
    this.pitchRange = 6; // number of octaves
    this.pitches = []; // main array for pitch data on y axis

    var kickLoaded = false;
    var allPitches = this.tonalEnv.getFullChordArray(1, 7, []);
    var lowestPitch = this.tonalEnv.key + (this.lowestOctave * 12);
    this.pitches = this.tonalEnv.trimArray(allPitches, lowestPitch, lowestPitch + (this.pitchRange * 12));

    // Callback that fires whenever a chord is about to change
    this.chordProgGen.pingOnChordChange((function(value) {
      //console.log("Ping for chord change " + message);
      var allNotes = this.tonalEnv.getFullChordArray(value.root, value.tochordtone, value.alterations);
      var lowestPitch = allNotes[0] + (this.lowestOctave * 12);
      var prevPitches = this.pitches;
      this.pitches = this.tonalEnv.trimArray(allNotes, lowestPitch, lowestPitch + (this.pitchRange * 12));
      //console.log("chord change " + this.pitches);
    }).bind(this));

}

/**
* Start the Tone transport running
**/
musicEnvironment.prototype.start = function() {
  Tone.getContext().resume();
  Tone.Transport.start("+0.1");
  console.log("Starting transport");
}

/**
* Pause Tone transport
**/
musicEnvironment.prototype.pauseTone = function() {
  Tone.Transport.pause();
  console.log("Pausing transport");
}


/**
 * Returns the full scale
 *
 **/

musicEnvironment.prototype.getFullScale = function() {
  return this.tonalEnv.getFullScale();
}

/**
* Sets the chord progression to play
* prog is a name like "1_2_3_4" fro the chordProggies object
**/
musicEnvironment.prototype.setChordProg = function(prog) {
    this.chordProgression = this.chordProggies.chordProgressions[prog];
    // remove old events
    this.chordProgPart.removeAll();
    for (var i = 0; i < this.chordProgression.length; i++) {
        this.chordProgPart.at(this.chordProgression[i].time, this.chordProgression[i]);
    }
}


/**
* Randomly select a chord progression from the chordProggies object
* @returns{element from chordProggies.chordProgressions[]} Returns a chordPRogression obhect
**/
musicEnvironment.prototype.getRandomChordProgression = function() {
    var randomProgNum = game.rnd.integerInRange(0, this.chordProggies.chordProgressions.length - 1);
    return this.chordProggies.chordProgressions[randomProgNum];
}

export default musicEnvironment
