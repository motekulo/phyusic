/**
 * Creates the matter canvas and adds objects to it
 **/

import * as Matter from 'matter-js'

var Events = Matter.Events;
var Engine = Matter.Engine;

function phyusicMatter() {
  this.init();
}

phyusicMatter.prototype.init = function() {
  //var elem = document.getElementById("matter");
  this.height = window.innerHeight,
  //this.width =  window.innerWidth;
  
  this.width = document.getElementById("matter").clientWidth * 0.9;
  this.players = []; // FIXME keep out of here

  var Render = Matter.Render,
  World = Matter.World,
  Composite = Matter.Composite,
  Composites = Matter.Composites,
  Common = Matter.Common,
  Constraint = Matter.Constraint,
  Vector = Matter.Vector,
  Mouse = Matter.Mouse,
  MouseConstraint = Matter.MouseConstraint,
  Body = Matter.Body,
  Bodies = Matter.Bodies;

  this.engine = Engine.create();
  //var renderElement = document.getElementById("mattter");
  var render = Render.create({
    element: document.getElementById("matter"),
    engine: this.engine,
    options: {height: window.innerHeight,
      width: document.getElementById("matter").clientWidth * 0.9,//window.innerWidth/12 * 9,    // hacky way to coordinate with material column layout
      wireframes: false}
    });

    this.engine.world.gravity.y = 0;

    var mouse = Mouse.create(render.canvas),
    mouseConstraint = MouseConstraint.create(this.engine, {
      mouse: mouse,
      constraint: {
        angularStiffness: 0.2,
        render: {
          visible: false
        }
      }
    });
    var self = this;
    var x, y = 0;

    Matter.Events.on(mouseConstraint, 'startdrag', function(event) {
      x = event.body.position.x;
      y = event.body.position.y;
    })

    Matter.Events.on(mouseConstraint, 'enddrag', function(event) {
      
      // Limit the velocity after a drag event so that bodies don't disappear #FIXME must be a better way
      if (event.body.velocity.x > 2) Matter.Body.setVelocity(event.body, { x: 2, y: event.body.velocity.y});
      if (event.body.velocity.x < -2) Matter.Body.setVelocity(event.body, { x: -2, y: event.body.velocity.y});
      if (event.body.velocity.y > 2) Matter.Body.setVelocity(event.body, { x: event.body.velocity.x, y: 2});
      if (event.body.velocity.y < -2) Matter.Body.setVelocity(event.body, { x: event.body.velocity.x, y: -2});

      console.log("Width is " + self.width + " and body x " + event.body.position.x);
      if (event.body.position.x > self.width || event.body.position.y > self.height) {
        Matter.Body.setPosition(event.body, { x: x, y: y });
      }
      if (event.body.position.x < 0 || event.body.position.y < 0) {
        Matter.Body.setPosition(event.body, { x: x, y: y });
      }
      
      // And stop it turning to make manipulation more predictable
      Matter.Body.setAngularVelocity(event.body, 0);
        
    });

    var lWall = Bodies.rectangle(4, this.height/2, 2, this.height, {isStatic: true, restitution: 1}),
    rWall = Bodies.rectangle(this.width - 8, this.height/2, 8, this.height, {isStatic: true, restitution: 1}),
    topWall = Bodies.rectangle(this.width/2, 8, this.width, 8, {isStatic: true, restitution: 1}),
    bottomWall = Bodies.rectangle(this.width/2, this.height - 8, this.width, 2, {isStatic: true, restitution: 1});

    // var bassBall = Bodies.circle(this.width/2, this.height/4 *3, this.height/12,
    //   {friction: 0.2, frictionAir: 0.1, restitution: 0.7,
    //     render: {
    //       fillStyle: '#d81b60'
    //     }});

    World.add(this.engine.world, [lWall, rWall, topWall, bottomWall,
          mouseConstraint]);

    Engine.run(this.engine);
    Render.run(render);

    Events.on(this.engine, 'beforeUpdate', function(event) {

    });

    render.mouse = mouse;

  }

phyusicMatter.prototype.addToWorld = function(composite) {
    Matter.World.add(this.engine.world, composite);
    return composite.id;
}

// FIXME shouldn't need this - players stay in touch with phyusic only
phyusicMatter.prototype.addMusicPlayer = function(player) {
    this.players.push(player);
}

phyusicMatter.prototype.removeComposite = function(id) {
    var object = Matter.Composite.get(this.engine.world, id, "composite");
    Matter.World.remove(this.engine.world, object);
}

phyusicMatter.prototype.removeBody = function(body) {
    //var object = Matter.Composite.get(this.engine.world, id, "composite");
    Matter.World.remove(this.engine.world, body);
}

phyusicMatter.prototype.pingOnCollision = function(x, callback) {
  // Event.on stuff in here?
  self = this;
  Events.on(self.engine, 'collisionStart', function(event) {
    callback(event.pairs);
  });
}

phyusicMatter.prototype.addBalls = function(color, label) {
    var numBalls = Nexus.ri(2,8);
    var ballDiameter = this.height/20;
    for (var i = 0; i < numBalls; i++) {
        var x = Nexus.rf(0,1) * this.width;
        var ball = Matter.Bodies.circle(x,
                    ballDiameter, ballDiameter/2,
                    {friction: 0.2, frictionAir: 0.1, restitution: 0.7,
                        label: label, render: {fillStyle: color}});
        ball.friction = 0.0005;
        //ball.playerIndex = playerIndex;
        //rectMaze = Matter.Composite.addBody(rectMaze, ball);
        Matter.World.add(this.engine.world, ball);
    }
}

phyusicMatter.prototype.addBassBall = function(x, y, color, label) {
    //var numBalls = Nexus.ri(2,8);
    var ballDiameter = this.height/20;
//for (var i = 0; i < numBalls; i++) {
    x = Nexus.rf(0,1) * this.width;
    y = y * this.height;
    var ball = Matter.Bodies.circle(x,
                y, ballDiameter/2,
                {friction: 0.2, frictionAir: 0.1, restitution: 0.7,
                    label: label, render: {fillStyle: color}});
    ball.friction = 0.0005;
    //ball.playerIndex = playerIndex;
    //rectMaze = Matter.Composite.addBody(rectMaze, ball);
    Matter.World.add(this.engine.world, ball);
    //}
}

export default phyusicMatter
