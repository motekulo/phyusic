/** @class
 * A chord generator pings the musicenvironment when a chord changes
 *
 **/

import * as Tone from 'tone'

import chordProggies from "../chordproggies";

 function ChordProgGenerator(proggy) {
   //this.chordProgression = [];
   this.init(proggy);
 }

ChordProgGenerator.prototype.init = function(proggy){
  this.chordProggies = new chordProggies();
  this.chordProgression = this.chordProggies.chordProgressions[proggy];

}


 ChordProgGenerator.prototype.pingOnChordChange = function(callback) {
   this.chordProgPart = new Tone.Part((function(time, value) {
     //console.log("Chord change");
     callback(value);
   }).bind(this), this.chordProgression);

   this.chordProgPart.loop = true;
   this.chordProgPart.loopEnd = this.chordProgression.length + "m";
   this.chordProgPart.start(0);
 }

// Given a new progression array index, changes the chord progression
ChordProgGenerator.prototype.changeProgression = function (newProgName) {
    this.chordProgression = this.chordProggies.chordProgressions[newProgName];
    this.chordProgPart.loopEnd = this.chordProgression.length + "m";
    for (var i = 0; i < this.chordProgression.length; i++ ) {
      this.chordProgPart.at(this.chordProgression[i].time, this.chordProgression[i]);
    }
}

 ////// This was originally in musicenvironment.js - might be of use for bigger sections
 //////
 // var chordForm = [
 //             {time: "7:3:3.0", // ie just before bar 8 for next round
 //              prog: this.chordProggies.chordProgressions["1_4_1_1"]},
 //             {time: "3:3:3.0", // just before bar 4
 //              prog: this.chordProggies.chordProgressions["1_4_5_5"]}
 //
 //          ];
 // //var self = this;
 // var chordFormPart = new Tone.Part((function(time, value){
 //     console.log("Progression change at " + Tone.Transport.position);
 //     //console.log(value);
 //     //this.chordProgression = value.prog;
 //     //var newValue = {};
 //     for (var i = 0; i < value.prog.length; i++) {
 //
 //         this.chordProgPart.at(value.prog[i].time, value.prog[i]);
 //     }
 //     this.chordProgPart.loopEnd = value.prog.length + "m";
 //     //console.log(this.chordProgression);
 // }).bind(this), chordForm);
 // chordFormPart.loop = true;
 // chordFormPart.loopEnd = "8m";  // FIXME currently manually adjusting
 //chordFormPart.start(0);

 export default ChordProgGenerator
