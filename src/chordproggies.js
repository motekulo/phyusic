/** @class
* A chordProggies provides chord progressions as used by the Tonality class
* and musicEnvironment
* @param {opbject} options - json object of options in the format
* @example
* options take the form of ...
**/
function chordProggies() {
    // init things here
}

/**
* Note that the length of the alterations array needs to reflect the value given
* by tochordtone. If tochordtone=5, then alterations should have three elements (1, 3 and 5);
* if it's 7, then length 4.
**/


chordProggies.prototype.chordProgressions = {"1" : [
        {time: "0m", root: 1, tochordtone: 5, alterations: [0,0,0,0]}
    ],
    "1_4_1_1": [
        {time: "3:3:3.0", root: 1, tochordtone: 5, alterations: [0,0,0]},
        {time: "0:3:3.0", root: 4, tochordtone: 5, alterations: [0,0,0]},
        {time: "1:3:3.0", root: 1, tochordtone: 5, alterations: [0,0,0]},
        {time: "2:3:3.0", root: 1, tochordtone: 7, alterations: [0,0,0,0]}
    ],
    "1_2_3_4": [
        {time: "3:3:3.0", root: 1, tochordtone: 5, alterations: [0,0,0]},
        {time: "0:3:3.0", root: 2, tochordtone: 5, alterations: [0,0,0]},
        {time: "1:3:3.0", root: 3, tochordtone: 5, alterations: [0,0,0]},
        {time: "2:3:3.0", root: 4, tochordtone: 5, alterations: [0,0,0,0]}
    ],
    "1_4_5_5": [
        {time: "3:3:3.0", root: 1, tochordtone: 5, alterations: [0,0,0]},
        {time: "0:3:3.0", root: 4, tochordtone: 5, alterations: [0,0,0]},
        {time: "1:3:3.0", root: 5, tochordtone: 5, alterations: [0,0,0]},
        {time: "2:3:3.0", root: 5, tochordtone: 7, alterations: [0,0,0,0]}
    ],
    "1_4_1_5": [
        {time: "3:3:3.0", root: 1, tochordtone: 5, alterations: [0,0,0]},
        {time: "0:3:3.0", root: 4, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "1:3:3.0", root: 1, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "2:3:3.0", root: 5, tochordtone: 7, alterations: [0,0,0,0]}
    ],
    "1_6_2_5": [
        {time: "3:3:3.0", root: 1, tochordtone: 5, alterations: [0,0,0]},
        {time: "0:3:3.0", root: 6, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "1:3:3.0", root: 2, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "2:3:3.0", root: 5, tochordtone: 7, alterations: [0,0,0,0]}
    ],
    "3_1": [
        {time: "1:3:3.0", root: 3, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "0:3:3.0", root: 1, tochordtone: 7, alterations: [0,0,0,0]}
    ],
    "hazy1": [
        {time: "3:3:3.0", root: 6, tochordtone: 7, alterations: [0,-1,0,-8]},
        {time: "0:3:3.0", root: 4, tochordtone: 7, alterations: [0,2,2,0]},
        {time: "1:3:3.0", root: 2, tochordtone: 9, alterations: [0,0,2,0,0]},
        {time: "2:3:3.0", root: 3, tochordtone: 7, alterations: [0,-3,0,0]}],
    "iii vi": [
        {time: "3:3:3.0", root: 3, tochordtone: 5, alterations: [0,0,0]},
        {time: "0:3:3.0", root: 3, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "1:3:3.0", root: 6, tochordtone: 5, alterations: [0,0,0,0]},
        {time: "2:3:3.0", root: 6, tochordtone: 7, alterations: [0,0,0,0]}],

    "ii VII7b5": [
        {time: "3:3:3.0", root: 2, tochordtone: 5, alterations: [0,0,0]},
        {time: "0:3:3.0", root: 2, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "1:3:3.0", root: 7, tochordtone: 5, alterations: [0,1,0,0]},
        {time: "2:3:3.0", root: 7, tochordtone: 7, alterations: [0,1,0,0]}],

    "hazy 2": [
        {time: "0m", root: 3, tochordtone: 5, alterations: [0,0,0]},
        {time: "0:3:3.0", root: 3, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "1:3:3.0", root: 6, tochordtone: 5, alterations: [0,0,0,0]},
        {time: "2:3:3.0", root: 6, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "3:3:3.0", root: 2, tochordtone: 5, alterations: [0,0,0]},
        {time: "4:3:3.0", root: 2, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "5:3:3.0", root: 7, tochordtone: 5, alterations: [0,1,0,0]},
        {time: "6:3:3.0", root: 7, tochordtone: 7, alterations: [0,1,0,0]}],
    "hazy 3": [
        {time: "0m", root: 6, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "0:3:3.0", root: 2, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "1:3:3.0", root: 6, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "2:3:3.0", root: 7, tochordtone: 7, alterations: [0,0,0,0]},
        {time: "3:3:3.0", root: 3, tochordtone: 7, alterations: [0,1,0,0]}]
        
                                             
}

export default chordProggies