/** @class
 * A simple monosynth instrument
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the instrument
 * 
 **/

import * as Tone from 'tone'

function monoSynthInstr(options) {
  this.init(options);
}

 monoSynthInstr.prototype.init = function(options) {
   
   this.modulator = null;
   var modulationData = [];
   // order that modulation occurs
   // options are pan, vol, attack, filter
   this.modulationOrder = ["pan", "vol", "filter", "attack"];

   this.send = new Tone.Volume(-12);
  // this.panVol = new Tone.PanVol(0,5, -12);
   this.channel = new Tone.Channel(-12, 0);
   this.sendVol = new Tone.Volume(-90);
   this.filter = new Tone.Filter(800, "lowpass");

   // Ranges for Modulation
   this.panRange = 1;
   this.volRange = -12;
   this.volMax = -6;
   this.filterLow = 50;
   this.filterRange = 8000;
   this.attackDiv = 1; // so 1 sec divided by 4 = 250ms slowest attack
 
   this.instrument = new Tone.MonoSynth({
      oscillator: {
          type: "square"
      },
      envelope: {
          attack: 0.1
      }
   });
   var self = this;
   

   // Poll periodically for modulation values
   const modulationPoll = new Tone.Loop((time) => {
	
     if (this.modulator != null) {
       modulationData = this.modulator.data();
       for (var i = 0; i < modulationData.length; i++) {
            this.mapModulation(this.modulationOrder[i], modulationData[i]);
            }
     }

   }, "1n").start(0); // polling every quarter note here

   this.instrument.set({curve: "linear"});
   
   this.instrument.chain(this.filter);
   this.filter.fan(this.send, this.channel);
   this.send.connect(this.sendVol); // not actually connecting the send yet
   
   this.channel.connect(Tone.Destination);
   //this.send.toDestination();
   
 }

monoSynthInstr.prototype.mapModulation = function(modulationType, data) {
  switch(modulationType) {
    case "pan": 
      this.channel.pan.rampTo(data * 2 * this.panRange - this.panRange, "4n");
      //console.log("Pan " + (data * 2 - 1));
      break;
      
    case "vol":
      this.channel.volume.rampTo(data * this.volRange + this.volMax, "4n");
      //console.log("Vol " + (data * this.volRange + this.volMax));
      break;
    
    case "filter":
      var freq = mapToLogFrequency(data, 50, 32000); // with the log scale, seem to need this much freq headroom to get 20k cutoff at far x
      this.filter.frequency.rampTo(freq, "4n");
      //console.log("Filter freq " + freq);
      break;
      
    case "attack":
      var attack = data/this.attackDiv;
      this.instrument.set({envelope: {attack: data},
                           filterEnvelope: {attack: data} 
                           });
      //instrument.envelope.attack
      //console.log("Attack " + this.instrument.envelope.attack);
      
  }
} 
 
monoSynthInstr.prototype.play = function(value, time) {
  var dur = "2m"; // default
  if (value.dur != null) {dur = value.dur};   
  this.instrument.triggerAttackRelease(value.note, dur, time);
}

 monoSynthInstr.prototype.dispose = function() {
   this.send.dispose();
   this.channel.dispose();
   this.instrument.dispose();
   if (this.modulator != null) {
     this.modulator.dispose();
   }

 }

function mapToLogFrequency(x, minHertz, maxHertz) {
  // Ensure x is within the range [0, 1]
  x = Math.min(Math.max(x, 0), 1);

  // Map the logarithmic value to the frequency range
  var logMin = Math.log10(minHertz);
  var logMax = Math.log10(maxHertz);

  var logValue = x * (logMax - logMin) + logMin;
  var frequency = Math.pow(10, logValue);

  return frequency;
}


 export default monoSynthInstr
