/** @class
 * A simple MembraneSynth instrument
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the instrument
 * 
 **/

import * as Tone from 'tone'

function metalSynthInstr(options) {
  this.init(options);
}

 metalSynthInstr.prototype.init = function(options) {
   
   this.modulator = null;
   var modulationData = [];
   // order that modulation occurs
   // options are pan, vol, attack, filter
   this.modulationOrder = ["pan", "vol", "filter", "attack"];

   this.send = new Tone.Volume(-12);
  // this.panVol = new Tone.PanVol(0,5, -12);
   this.channel = new Tone.Channel(-12, 0);
   this.sendVol = new Tone.Volume(-90);
   this.filter = new Tone.Filter(800, "lowpass");

   // Ranges for Modulation
   this.panRange = 1;
   this.volRange = -12;
   this.filterLow = 50;
   this.filterRange = 8000;
   this.attackDiv = 4; // so 1 sec divided by 4 = 250ms slowest attack
 
   this.instrument = new Tone.MetalSynth();
   var self = this;
   

   // Poll periodically for modulation values
   const modulationPoll = new Tone.Loop((time) => {
	
     if (this.modulator != null) {
       modulationData = this.modulator.data();
       for (var i = 0; i < modulationData.length; i++) {
            this.mapModulation(this.modulationOrder[i], modulationData[i]);
            }
     }

   }, "4n").start(0); // polling every quarter note here

   this.instrument.set({curve: "linear"});
   
   this.instrument.chain(this.filter);
   this.filter.fan(this.send, this.channel);
   this.send.connect(this.sendVol); // not actually connecting the send yet
   
   this.channel.connect(Tone.Destination);
   //this.send.toDestination();
   
 }

metalSynthInstr.prototype.mapModulation = function(modulationType, data) {
  switch(modulationType) {
    case "pan": 
      this.channel.pan.rampTo(data * 2 * this.panRange - this.panRange, "4n");
      //console.log("Pan " + (data * 2 - 1));
      break;
      
    case "vol":
      this.channel.volume.rampTo(data * this.volRange - 6, "4n");
      //console.log("Vol " + (data * -15 - 3));
      break;
    
    case "filter":
      this.filter.frequency.rampTo(data * this.filterRange + this.filterLow, "4n");
      //console.log("Filter freq " + (data * 8000 + 50));
      break;
      
    case "attack":
      var attack = data/this.attackDiv;
      this.instrument.envelope.attack = attack;
      //this.instrument.set({attack: attack});
      //console.log("Attack " + attack);
      
  }
} 
 
metalSynthInstr.prototype.play = function(value, time) {
  var dur = "8n"; // default
  if (value.dur != null) {dur = value.dur}; 
  this.instrument.triggerAttackRelease("C6", dur, time);
  //this.instrument.triggerAttackRelease(value.note, dur, time);
}

 metalSynthInstr.prototype.dispose = function() {
   this.send.dispose();
   this.channel.dispose();
   this.instrument.dispose();
   if (this.modulator != null) {
     this.modulator.dispose();
   }

 }

 export default metalSynthInstr
