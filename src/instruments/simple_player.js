/** @class
 * A simple pitched sampler instrument
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the instrument
 * options.sample -
 **/

import * as Tone from 'tone'

function simplePlayer(options) {
  this.init(options);
}

 simplePlayer.prototype.init = function(options) {
   this.sample = options.sample;  // array
   this.modulator = null;
   var modulationData = [];
   // order that modulation occurs
   // options are pan, vol, attack, filter
   this.modulationOrder = ["pan", "vol", "filter", "attack"];

   this.send = new Tone.Volume(-12);
  // this.panVol = new Tone.PanVol(0,5, -12);
   this.channel = new Tone.Channel(-12, 0);
   this.sendVol = new Tone.Volume(-90);
   this.filter = new Tone.Filter(800, "lowpass");
   this.instrument = new Tone.Player({}, (function(){
       //this.pitchedSampleLoaded = true;
       //console.log("pitched sample loaded");
   }).bind(this));
   var self = this;
   
   loadSample(this.sample);
   

   function loadSample(sample){
     var xhr = new XMLHttpRequest();
     var location = sample.location;
     xhr.open("GET", location, true);
     xhr.responseType = 'blob';

     xhr.onload = function(){
       sampleBlob = URL.createObjectURL(this.response);
       
       self.instrument.load(sampleBlob);
       
//       self.instrument.add(sample.pitch, sampleBlob, (function(){
//           self.pitchedSampleLoaded = true;
//           console.log("sample loaded");
//       }).bind(this));
     };
     xhr.send();
   }

   // Poll periodically for modulation values
   const modulationPoll = new Tone.Loop((time) => {
	
     if (this.modulator != null) {
       modulationData = this.modulator.data();
       for (var i = 0; i < modulationData.length; i++) {
            this.mapModulation(this.modulationOrder[i], modulationData[i]);
            }
     }

   }, "4n").start(0); // polling every quarter note here

   this.instrument.set({curve: "linear"});
   
   this.instrument.chain(this.filter);
   this.filter.fan(this.send, this.channel);
   this.send.connect(this.sendVol); // not actually connecting the send yet
   
   this.channel.connect(Tone.Destination);
   //this.send.toDestination();
   
 }

simplePlayer.prototype.mapModulation = function(modulationType, data) {
  switch(modulationType) {
    case "pan": 
      this.channel.pan.rampTo(data * 2 - 1, "4n");
      //console.log("Pan " + (data * 2 - 1));
      break;
      
    case "vol":
      this.channel.volume.rampTo(data * -15 - 3, "4n");// 
      break;
    
    case "filter":
      this.filter.frequency.rampTo(data * 8000 + 50, "4n");
      //console.log("Filter freq " + (data * 8000 + 50));
      break;
      
    case "attack":
      var attack = data/4;
      this.instrument.set({attack: attack});
      //console.log("Attack " + attack);
      
  }
} 
 
simplePlayer.prototype.play = function(value, time) {
  var dur = "2m"; // default
  if (value.dur != null) {dur = value.dur};   
  //this.instrument.triggerAttackRelease(value.note, dur, time);
  if (this.instrument.loaded === true) {
    this.instrument.start(time);
  }
  
}

 simplePlayer.prototype.dispose = function() {
   this.send.dispose();
   this.channel.dispose();
   this.instrument.dispose();
   if (this.modulator != null) {
     this.modulator.dispose();
   }

 }
