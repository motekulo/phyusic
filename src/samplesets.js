/** @class
* A sampleSet provides samples and metadata in set as used by the musicEnvironment
*
**/
function SampleSet() {
    // init things here
}

/**
*
**/

SampleSet.prototype.sets =
    {"bass": [{"pitch": "C3",
                             "location" : "assets/samples/big_trancy/bass.wav"}],
      "pad": [{"pitch": "C4",
                            "location" : "assets/samples/big_trancy/pad.wav"}],
      "synth": [{"pitch": "C5",
                           "location" : "assets/samples/big_trancy/lead.wav"}],
      "kick": [{"pitch": "C1",
                          "location" : "assets/samples/big_trancy/kick.wav"}],
      "clap": [{"pitch": "",
                         "location" : "assets/samples/big_trancy/clap.wav"}],
     
   "doublebass": [{"pitch": "C2",
                     "location" : "assets/samples/jazzy/double_bass_c2.wav"}],
    "elecpiano": [{
                    "pitch": "C4",
                    "location" : "assets/samples/jazzy/elecpiano_c4.wav"}],
    "marimba": [{
                    "pitch": "C4",
                    "location" : "assets/samples/jazzy/marimba_c4.wav"}],
    "marimba2": [{
                    "pitch": "C4",
                    "location" : "assets/samples/jazzy/marimba_c4_trimmed.wav"}],               
    "clave": [{"pitch": "C3",
                        "location" : "assets/samples/perc/clave.wav"}],
    "clicker": [{"pitch": "C3",
                    "location" : "assets/samples/perc/clicker.wav"}],
    "conga1": [{"pitch": "C3",
                    "location" : "assets/samples/perc/conga1.wav"}],
    "conga2": [{"pitch": "",
                    "location" : "assets/samples/perc/conga2.wav"
                  }],
    "tight_bass": [{"pitch": "C2",
                     "location": "assets/samples/bass/tight_bass_c2.wav"},
                     {"pitch": "C3",
                      "location" : "assets/samples/bass/tight_bass_c3.wav"},
                     {"pitch": "C4",
                     "location" : "assets/samples/bass/tight_bass_c4.wav"}
                 ],
      "areshetech_bass": [ {"pitch": "C1",
                   "location": "assets/samples/bass/bass_areshetech_c1.wav"},
                  {"pitch": "C2",
                   "location": "assets/samples/bass/bass_areshetech_c2.wav"}
              ],
     "pluck_bell": [{"pitch": "C2",
                      "location" : "assets/samples/bell/pluck_bell_c2.wav"},
                      {"pitch": "C3",
                       "location" : "assets/samples/bell/pluck_bell_c3.wav"},
                      {"pitch": "C4",
                      "location" : "assets/samples/bell/pluck_bell_c4.wav"},
                      {"pitch": "C5",
                      "location" : "assets/samples/bell/pluck_bell_c5.wav"}
                  ],
     
     "blip": [{"pitch": "C3",
                      "location" : "assets/samples/synth/blip_c3.wav"},
                      {"pitch": "C4",
                      "location" : "assets/samples/synth/blip_c4.wav"}
                  ],
     
      "drone1": [{"pitch": "G1",
                       "location" : "assets/samples/drone/hg_g1.wav"},
                        {"pitch": "D2",
                        "location" : "assets/samples/drone/hg_d2.wav"}
                   ],
      "windy_drone": [{"pitch": "C3",
                       "location" : "assets/samples/drone/windy_c3.wav"},
                      {"pitch": "G3",
                       "location" : "assets/samples/drone/windy_g3.wav"},
                       {"pitch": "C4",
                        "location" : "assets/samples/drone/windy_c4.wav"}],
     
     "pad_1": [{"pitch": "C1",
                       "location" : "assets/samples/pad/pad_1-c1.wav"},
                      {"pitch": "C2",
                       "location" : "assets/samples/pad/pad_1-c2.wav"},
                       {"pitch": "C3",
                       "location" : "assets/samples/pad/pad_1-c3.wav"},
               {"pitch": "C4",
                       "location" : "assets/samples/pad/pad_1-c4.wav"}],
     "wavy_bass": [{"pitch": "C1",
                       "location" : "assets/samples/bass/wavy_bass-c1.wav"},
                      {"pitch": "C2",
                       "location" : "assets/samples/bass/wavy_bass-c2.wav"},
                       {"pitch": "C3",
                       "location" : "assets/samples/bass/wavy_bass-c3.wav"},
               {"pitch": "C4",
                       "location" : "assets/samples/bass/wavy_bass-c4.wav"}],
     "freakpad_1": [{"pitch": "C1",
                       "location" : "assets/samples/pad/freakpad_1-c1.wav"},
                      {"pitch": "C2",
                       "location" : "assets/samples/pad/freakpad_1-c2.wav"},
                       {"pitch": "C3",
                       "location" : "assets/samples/pad/freakpad_1-c3.wav"},
               {"pitch": "C4",
                       "location" : "assets/samples/pad/freakpad_1-c4.wav"}],
     "freakpad_2": [{"pitch": "C1",
                       "location" : "assets/samples/pad/freakpad_2-c1.wav"},
                      {"pitch": "C2",
                       "location" : "assets/samples/pad/freakpad_2-c2.wav"},
                       {"pitch": "C3",
                       "location" : "assets/samples/pad/freakpad_2-c3.wav"},
                      {"pitch": "C4",
                       "location" : "assets/samples/pad/freakpad_2-c4.wav"}],
     "organ_1": [{"pitch": "C1",
                       "location" : "assets/samples/synth/organ_1-c1.wav"},
                      {"pitch": "C2",
                       "location" : "assets/samples/synth/organ_1-c2.wav"},
                       {"pitch": "C3",
                       "location" : "assets/samples/synth/organ_1-c3.wav"},
                      {"pitch": "C4",
                       "location" : "assets/samples/synth/organ_1-c4.wav"}],
     "hat1": [{"pitch": "",
                       "location" : "assets/samples/perc/hat1.wav"}],
     "hat2": [{"pitch": "",
                       "location" : "assets/samples/perc/hat2.wav"}],
     "kick1": [{"pitch": "",
                       "location" : "assets/samples/perc/kick1.wav"}],
     "kick2": [{"pitch": "",
                       "location" : "assets/samples/perc/kick2.wav"}],
     "snare1": [{"pitch": "",
                       "location" : "assets/samples/perc/snare1.wav"}]
     
    };

    export default SampleSet
