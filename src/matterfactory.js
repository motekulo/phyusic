/** @class
* A matterFactory creates a range of custom matter js composites
*
**/

import * as Matter from 'matter-js'

function matterFactory() {
  //this.init(options);
}

/**
* @todo Is this used anywhere at the moment?
**/
matterFactory.prototype.selectComposite = function(name, options) {
    var composite = {};
    switch (name) {
        case 'threeballs':
            composite = this.threeBalls(options);
            return composite;
            break;

        case 'ball':
            composite = this.oneBall(options);
            return composite;
            break;
    }
}

/**
* Create a number of rows of blocks
* @param {object} - options.width, options.height.
* @todo numRows is hard coded - should be added to the options
**/
matterFactory.prototype.ballFallMaze = function(options){
    var rectWidth = options.width/24,
        rectHeight = options.height/20;

    // Build a grid/maze of rectangles
    var rectMaze = Matter.Composite.create({ label: 'rectmaze'});
    var numRows = 6;
    for (var i = 0; i < numRows; i++) {
        var numRects = Nexus.ri(6,12);
        var xOffset = Nexus.ri(1,5) * 10;
        var y = options.height/(numRows + 1) * i + rectHeight * 3;
        for (var k = 0; k < (numRects + 1); k++) {
            var x = options.width/numRects * k + rectWidth/2 + xOffset;
            var slope = Nexus.pick(-1, 1) * Math.PI * 0.05;
            var rect = Matter.Bodies.rectangle(x, y, rectWidth, rectHeight,
                                {isStatic: true, angle: slope,
                                friction: 0.1 });
            rect.friction = 0.001;
            rectMaze = Matter.Composite.addBody(rectMaze, rect);
        }

    }
    return rectMaze;
}

/**
* Add a single ball
* @param {object} options - options.x, options.y, options.size
**/
matterFactory.prototype.oneBall = function(options) {
    var x = options.x,
        y = options.y,
        size = options.size,
        color = options.color;
        if (color == null) color = '#1e88e5';
        var oneBall = Matter.Composite.create({ label: 'oneball'});

        var ball1 = Matter.Bodies.circle(x, y, size/24, {friction: 0.1, frictionAir: 0.02, frictionStatic: 0.5,
                                                          restitution: 0.5, render: {fillStyle: color}});
        
        oneBall = Matter.Composite.addBody(oneBall, ball1);
        return oneBall;
}

/**
* Add a single ball and a square
* @param {object} options - options.x, options.y, options.size
**/
matterFactory.prototype.oneBallOneSquare = function(options) {
    var x = options.x,
        y = options.y,
        size = options.size,
        color = options.color;
        if (color == null) color = '#1e88e5';
        var comp = Matter.Composite.create({ label: 'oneballonesquare'});
        var ball1 = Matter.Bodies.circle(x, y, size/24,
                    {friction: 0.1, frictionAir: 1, frictionStatic: 0.5, restitution: 1, render: {
                      fillStyle: color}});
        var square = Matter.Bodies.rectangle(x, y, size/12, size/12,
                    {friction: 0.1, frictionAir: 0.02, frictionStatic: 0.5, restitution: 1, render: {
                      fillStyle: color}});
        comp = Matter.Composite.addBody(comp, ball1);
        comp = Matter.Composite.addBody(comp, square);
        return comp;
}

/**
* Add three balls
* @param {object} options - options.x, options.y, options.size
* @todo Why not have an addXBalls function, and call oneBall x times?
**/
matterFactory.prototype.threeBalls = function(options) {
    var x = options.x,
        y = options.y,
        size = options.size;
    var threeBalls = Matter.Composite.create({ label: 'threeballs'});

    var ball1 = Matter.Bodies.circle(x, y, size/24,
                {friction: 0.2, frictionAir: 0.1, restitution: 0.7, render: {
                fillStyle: '#1e88e5'
                  }});
    var ball2 = Matter.Bodies.circle(x, y, size/24,
                {friction: 0.2, frictionAir: 0.1, restitution: 0.7, render: {
                fillStyle: '#fdd835'
                  }});
    var ball3 = Matter.Bodies.circle(x, y, size/24,
                {friction: 0.2, frictionAir: 0.1, restitution: 0.7, render: {
                fillStyle: '#00acc1'
                  }});

    threeBalls = Matter.Composite.addBody(threeBalls, ball1);
    threeBalls = Matter.Composite.addBody(threeBalls, ball2);
    threeBalls = Matter.Composite.addBody(threeBalls, ball3);

    return threeBalls;

}

matterFactory.prototype.pendulum = function(options) {
  var x = options.x,
  y = options.y,
  size = options.size;
  var body = Matter.Bodies.rectangle(x, y, size/10, size);
    //var ball = Matter.Bodies.circle(550, 150, 20);

  var pendulum = Matter.Composite.create({ label: 'pendulum'});

  var constraint = Matter.Constraint.create({
    pointA: { x: x, y: y },
    bodyB: body,
    pointB: {x: 0, y: -(size/2 - 10)},
    length: 0
  });
    pendulum = Matter.Composite.add(pendulum, body);
    //pendulum = Matter.Composite.add(pendulum, ball);
    pendulum = Matter.Composite.add(pendulum, constraint);

    return pendulum;
  //  World.add(world, [body, ball, constraint]);
}

matterFactory.prototype.lfoBall = function(options) {
  var x = options.x,
      y = options.y,
      size = options.size,
      color = options.color;
  if (color == null) color = '#1e88e5';;
  var lfoBallComposite = Matter.Composite.create({label: "lfoball"});
  var containerBall = Matter.Bodies.circle(x, y, size,
      {friction: 0, frictionAir: 0, restitution: 1, render: {fillStyle: color}});
  var smallBall = Matter.Bodies.circle(x - (size/2), y, size/8,
      {friction: 0, frictionAir: 0, restitution: 1, render: {fillStyle: '#1e88e5'}});
  
  var lfoBall = Matter.Body.create({
    parts: [containerBall, smallBall]
  });
  Matter.Composite.addBody(lfoBallComposite, lfoBall);
  return lfoBallComposite;
  
}

matterFactory.prototype.spinnerThing = function(options) {
  var x = options.x,
      y =options.y,
      size = options.size;
  var spinner = Matter.Composite.create({label: "spinner"});
  var rect1 = Matter.Bodies.rectangle(x, y, size/8, size);
  var rect2 = Matter.Bodies.rectangle(x, y, size, size/8);
  var spinnerBody = Matter.Body.create({parts: [rect1, rect2]});
  
  Matter.Composite.addBody(spinner, spinnerBody);
  return spinner;
  
  
}

matterFactory.prototype.staticRect = function(options) {
  var x = options.x,
      y =options.y,
      width = options.width,
      height = options.height,
      slope = options.slope;
  var staticRect = Matter.Composite.create({label: "staticrect"});
  
  var rect = Matter.Bodies.rectangle(x, y, width, height, { isStatic: true, angle: Math.PI * slope, render: { fillStyle: options.color }});
  Matter.Composite.addBody(staticRect, rect);
  return staticRect;
  
}

export default matterFactory
  