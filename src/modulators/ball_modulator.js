/** @class
 * A simple ball modulator
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the modulator
 * options.sample -
 **/

import * as Nexus from 'nexusui'
import * as Matter from 'matter-js'

import matterFactory from '../matterfactory';


 function ballModulator(options) {
   this.init(options);
 }

 ballModulator.prototype.init = function(options) {
   this.world = options.world;
   this.label = options.label;
   this.color = options.color;
   this.factory = new matterFactory();

   this.xOrigin = 0;
   this.yOrigin = 0;
   this.xBoundary = this.world.width;
   this.yBoundary = this.world.height;

   //this.numBalls = options.numballs;
   if (options.size != null) {var size = options.size} else {var size = 240};
   
    if (options.x == "random" || options.x == null) {
     options.x = this.world.width * Nexus.rf(0, 1);
   }
   
   if (options.y == "random" || options.y == null) {
     options.y = this.world.height * Nexus.rf(0, 1);
   }
   
   //options.size = 240;
   
//   var x = this.world.width * Nexus.rf(0, 1);
//   var y = this.world.height * Nexus.rf(0, 1);
//   const ballOptions = {
//     "x": x,
//     "y": y,
//     "size": size,
//     "color": this.color
//   }
   this.composite = this.factory.oneBall(options);
   for (var i = 0; i < this.composite.bodies.length; i++) {
     Matter.Body.setVelocity(this.composite.bodies[i], {x: Nexus.rf(-2, +2),
                                                         y: Nexus.rf(-2, +2)});
     // Matter.Body.setMass(this.composite.bodies[i], 10);

 }
   this.world.addToWorld(this.composite);
   this.modulationData = [0, 0];

 }

ballModulator.prototype.data = function() {
  this.modulationData[0] = (this.composite.bodies[0].position.x - this.xOrigin)/(this.xBoundary - this.xOrigin);
  this.modulationData[1] = (this.composite.bodies[0].position.y - this.yOrigin)/(this.yBoundary - this.yOrigin);
  for (var i = 0; i < this.modulationData.length; i++) {
    if (this.modulationData[i] > 1) this.modulationData[i] = 1;
    if (this.modulationData[i] < 0) this.modulationData[i] = 0;
  }
  
  return this.modulationData;
}

ballModulator.prototype.moveIt = function() {
  var x = this.composite.bodies[0].velocity.x;
  var y = this.composite.bodies[0].velocity.y;
  var d = Nexus.rf(-2, +2);
  
  Matter.Body.setVelocity(this.composite.bodies[0], {x: x + d,
                                                     y: y + d});
}

ballModulator.prototype.setFriction = function (mfriction, mfrictionAir, mfrictionStatic, mrestitution) {
  this.composite.bodies[0].friction = mfriction;
  this.composite.bodies[0].frictionAir = mfrictionAir;
  this.composite.bodies[0].frictionStatic = mfrictionStatic;
  this.composite.bodies[0].restitution = mrestitution;
}

ballModulator.prototype.dispose = function() {
  console.log("Dispose of ball modulator");
  Matter.World.remove(this.world.engine.world, this.composite);
}

export default ballModulator
