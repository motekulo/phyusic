/** @class
 * A simple ball and square modulator
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the modulator
 * options.sample -
 **/

import matterFactory from "../matterfactory";
import * as Matter from 'matter-js'
import * as Nexus from 'nexusui'

 function ballSquareModulator(options) {
   this.init(options);
 }

 ballSquareModulator.prototype.init = function(options) {
   this.world = options.world;
   this.label = options.label;
   this.color = options.color;
   this.factory = new matterFactory();

   this.xOrigin = 0;
   this.yOrigin = 0;
   this.xBoundary = this.world.width;
   this.yBoundary = this.world.height;

   //this.numBalls = options.numballs;

   if (options.x == "random" || options.x == null) {
     options.x = this.world.width * Nexus.rf(0, 1);
   }
   
   if (options.y == "random" || options.y == null) {
     options.y = this.world.height * Nexus.rf(0, 1);
   }
   
   options.size = 240;
   
//   const ballOptions = {
//     "x": x,
//     "y": y,
//     "size": 240,
//     "color": this.color
//   }
  this.composite = this.factory.oneBallOneSquare(options);
  for (var i = 0; i < this.composite.bodies.length; i++) {
    Matter.Body.setVelocity(this.composite.bodies[i], {x: Nexus.rf(-2, +2),
                                                        y: Nexus.rf(-2, +2)});
    //Matter.Body.setMass(this.composite.bodies[i], 100);
  }
  
   this.world.addToWorld(this.composite);
   this.modulationData = [0, 0, 0, 0];

 }

ballSquareModulator.prototype.data = function() {
  this.modulationData[0] = (this.composite.bodies[0].position.x - this.xOrigin)/(this.xBoundary - this.xOrigin);
  this.modulationData[1] = (this.composite.bodies[0].position.y - this.yOrigin)/(this.yBoundary - this.yOrigin);
  this.modulationData[2] = (this.composite.bodies[1].position.x - this.xOrigin)/(this.xBoundary - this.xOrigin);
  this.modulationData[3] = (this.composite.bodies[1].position.y - this.yOrigin)/(this.yBoundary - this.yOrigin);

  for (var i = 0; i < this.modulationData.length; i++) {
    if (this.modulationData[i] > 1) this.modulationData[i] = 1;
    if (this.modulationData[i] < 0) this.modulationData[i] = 0;
  }
  return this.modulationData;
}

ballSquareModulator.prototype.setFriction = function (mfriction, mfrictionAir, mfrictionStatic, mrestitution) {
  for (var i = 0; i < this.composite.bodies.length; i++) {
    this.composite.bodies[i].friction = mfriction;
    this.composite.bodies[i].frictionAir = mfrictionAir;
    this.composite.bodies[i].frictionStatic = mfrictionStatic;
    this.composite.bodies[i].restitution = mrestitution;
  }
}

ballSquareModulator.prototype.moveIt = function() {
  var x = this.composite.bodies[0].velocity.x;
  var y = this.composite.bodies[0].velocity.y;
  var d = Nexus.rf(-2, +2);
  
  Matter.Body.setVelocity(this.composite.bodies[0], {x: x + d,
                                                     y: y + d});
}

ballSquareModulator.prototype.dispose = function() {
  console.log("Dispose of ball square modulator");
  Matter.World.remove(this.world.engine.world, this.composite);
}



export default ballSquareModulator
