/** @class
 * A simple ball modulator
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the modulator
 * options.sample -
 **/

 function spinnerModulator(options) {
   this.init(options);
 }

 spinnerModulator.prototype.init = function(options) {
   this.world = options.world;
   this.label = options.label;
   this.color = options.color;
   //this.numBalls = options.numballs;

   const ballOptions = {
     "x": 36,
     "y": 72,
     "size": options.size,
     "color": this.color
   }
   
   this.composite = matterFactory.spinnerThing(ballOptions);
   for (var i = 0; i < this.composite.bodies.length; i++) {
     Matter.Body.setVelocity(this.composite.bodies[i], {x: Nexus.rf(-2, +2),
                                                         y: Nexus.rf(-2, +2)})
  }
   this.world.addToWorld(this.composite);
   this.modulationData = [0, 0];

 }

spinnerModulator.prototype.data = function() {
  this.modulationData[0] = this.composite.bodies[0].position.x/this.world.width;
  this.modulationData[1] = this.composite.bodies[0].position.y/this.world.height;
  for (var i = 0; i < this.modulationData.length; i++) {
    if (this.modulationData[i] > 1) this.modulationData = 1;
    if (this.modulationData[i] < 0) this.modulationData = 0;
  }
  return this.modulationData;
}

spinnerModulator.prototype.moveIt = function() {

  var d = Nexus.rf(-2, +2);
  
  
  Matter.Body.setAngularVelocity(this.composite.bodies[0], 0.1);
  
  var forceMagnitude = 0.01 * this.composite.bodies[0].mass;
//  var x = (forceMagnitude + Nexus.rf(0,1) * forceMagnitude) * Nexus.pick(1, -1); 
//  var y =  -forceMagnitude + Nexus.rf(0,1) * -forceMagnitude;

  //console.log("Force x: " + x + " and y " + y);
  Matter.Body.applyForce(this.composite.bodies[0], this.composite.bodies[0].position, {
      x: (forceMagnitude + Nexus.rf(0,1) * forceMagnitude) * Nexus.pick(1, -1), 
      y: -forceMagnitude + Nexus.rf(0,1) * -forceMagnitude
  });
  
  Matter.Body.setAngularVelocity(this.composite.bodies[0], 0.1);
  
}

spinnerModulator.prototype.dispose = function() {
  console.log("Dispose of ball modulator");
  Matter.World.remove(this.world.engine.world, this.composite);
}
