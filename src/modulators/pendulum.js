/** @class
 * A simple pendulum modulator
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the modulator
 * options.sample -
 **/

 function pendulumModulator(options) {
   this.init(options);
 }

 pendulumModulator.prototype.init = function(options) {
   this.world = options.world;
   this.label = options.label;
   this.color = options.color;
   //this.numBalls = options.numballs;

   const pendulumOptions = {
     "x": options.x,
     "y": options.y,
     "size": options.size
   }
   this.composite = matterFactory.pendulum(pendulumOptions);
   this.world.addToWorld(this.composite);

   this.modulationData = [0, 0];

 }

pendulumModulator.prototype.data = function() {
  //this.modulationData[0] = this.ball.bodies[0].position.x/this.world.width;
  //this.modulationData[1] = this.ball.bodies[0].position.y/this.world.height;
  var x = this.composite.bodies[0].position.x;
  var y = this.composite.bodies[0].position.y;
  console.log("Pendulum mod x, y: " + x + ", " + y);
  return [0,0];
  //return this.modulationData;
}

pendulumModulator.prototype.dispose = function() {
  //console.log("Dispose of pendulum modulator");
  Matter.World.remove(this.world.engine.world, this.composite);
}
