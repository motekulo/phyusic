/**
 * Pneum - part of the January 2024 set of pieces
 **/

import * as Tone from 'tone';
import * as Nexus from 'nexusui';
import ballModulator from '../modulators/ball_modulator';
import ballSquareModulator from '../modulators/ball_square_modulator';
import simplePartGenerator from '../note_generators/simple_part';
import patternGenerator from '../note_generators/pattern';
import monoSynthInstr from '../instruments/monosynth';
import simpleBeatGenerator from '../note_generators/simplebeat';
import membraneSynthInstr from '../instruments/membranesynth';
import metalSynthInstr from '../instruments/metalsynth';
import matterFactory from '../matterfactory';


var colorPointer = 0;
var colors = ["#e57373", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7",
                    "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176",
                    "#ffd54f", "#ffb74d", "#ff8a65", "#a1887f", "#e0e0e0", "#90a4ae"];

var kicktoggle = "";
var hattoggle = "";
var pattern1toggle = "";
var pattern2toggle = "";
var arp1Osc = "";
var arp2Osc = "";
var multislider = "";

function lakaPiece(options) {
    this.init(options);
}

lakaPiece.prototype.init = function(options) {
    this.enviro  = options.enviro;
    this.world = options.world;
    this.factory = new matterFactory();
    this.noteGens = [];
    this.kick = "";
    this.closedhat = "";
    this.arp1 = "";
    this.arp2 = "";
    this.bass = "";
    var periods = ["2n", "3n", "4n", "6n", "8n", "16n"];  

    // Nexus UI interface toggles specific to piece
    kicktoggle = new Nexus.Toggle('#slot1',{
      'size': [40,20],
      'state': false
    })
    var self = this;
    kicktoggle.on('change',function(v) {
      if (v === true) {
        self.kick.start();
      }
      if (v === false) {
        self.kick.stop();
      }
    });
    document.getElementById("slot1label").innerHTML = "Kick";

    hattoggle = new Nexus.Toggle('#slot2',{
      'size': [40,20],
      'state': false
    })
    var self = this;
    hattoggle.on('change',function(v) {
      if (v === true) {
        self.closedhat.start();
      }
      if (v === false) {
        self.closedhat.stop();
      }
    });
    document.getElementById("slot2label").innerHTML = "Hat 1";


    hattoggle = new Nexus.Toggle('#slot3',{
      'size': [40,20],
      'state': false
    })
    var self = this;
    hattoggle.on('change',function(v) {
      if (v === true) {
        self.closedhat2.start();
      }
      if (v === false) {
        self.closedhat2.stop();
      }
    });
    document.getElementById("slot3label").innerHTML = "Hat 2";

    pattern1toggle = new Nexus.Toggle('#slot4',{
      'size': [40,20],
      'state': false
    })
    //var self = this;
    pattern1toggle.on('change',function(v) {
      if (v === true) {
        self.arp1.start();
      }
      if (v === false) {
        self.arp1.stop();
      }
    });
    document.getElementById("slot4label").innerHTML = "Pattern 1";

    pattern2toggle = new Nexus.Toggle('#slot5',{
      'size': [40,20],
      'state': false
    })
    var self = this;
    pattern2toggle.on('change',function(v) {
      if (v === true) {
        self.arp2.start();
      }
      if (v === false) {
        self.arp2.stop();
      }
    });

    document.getElementById("slot5label").innerHTML = "Pattern 2";

    arp1Osc = new Nexus.Select('#slot6',{
      'size': [100,30],
      'options': ["amsine11", "amsine24", "amsquare7", "amsawtooth17", "amtriangle14"]
    });
    
    arp1Osc.on('change', function(v) {
      self.arp1.instrument.instrument.oscillator.type = v.value;
  
    });
    document.getElementById("slot6label").innerHTML = "Pattern 1 Oscillator";
  
    arp2Osc = new Nexus.Select('#slot7',{
      'size': [100,30],
      'options': ["fmsine", "fmsquare", "fmsquare7", "fmsawtooth13", "fmtriangle21"]
    });
    
    arp2Osc.on('change', function(v) {
      self.arp2.instrument.instrument.oscillator.type = v.value;
    });
    document.getElementById("slot7label").innerHTML = "Pattern 2 Oscillator";

    multislider = new Nexus.Multislider('#slot8',{
      'size': [200,100],
      'numberOfSliders': 4,
      'min': 0,
      'max': 1,
      'step': 0,
      'candycane': 3,
      'values': [0.9, 0.8, 0.7, 0.6, 0.5],
      'smoothing': 0,
      'mode': 'bar'  // 'bar' or 'line'
     })

     multislider.on('change',function(v) {
      console.log(v);
      self.pingpongModulator.setFriction(v[0], v[1], v[2], v[3]);
      for (var i = 0; i < self.noteGens.length; i++) {
        if (self.noteGens[i].modulator != null) {
          self.noteGens[i].modulator.setFriction(v[0], v[1], v[2], v[3]);
        }
        if (self.noteGens[i].instrument.modulator != null) {
          self.noteGens[i].instrument.modulator.setFriction(v[0], v[1], v[2], v[3]);
        }
      }
    })

    options.world.engine.world.gravity.y = 0;
  
    var rectOptions = {
      "x": options.world.width/2,
      "y": options.world.height/2,
      "width": options.world.width,
      "height": 10,
      "slope": 0
    }
    
    
    rectOptions.color = options.color;
    this.rect1 = this.factory.staticRect(rectOptions);
    options.world.addToWorld(this.rect1);

    rectOptions.width = 10;
    rectOptions.height = options.world.height;
    rectOptions.x = options.world.width/2;
    rectOptions.y = options.world.height/2;

    this.rect2 = this.factory.staticRect(rectOptions);
    options.world.addToWorld(this.rect2);
    
    // Reverb
    this.reverb = new Tone.Reverb(8).toDestination();
  
    // Delay
    this.pingpongDelay = new Tone.PingPongDelay("8n", 0.5).toDestination();
    
    options.color = "#e57373";
    options.size = 400;
    options.x = options.world.width/4 * 3;
    options.y = options.world.height/4;
    this.pingpongModulator = new ballModulator(options);
    this.pingpongModulator.xOrigin = options.world.width/2;
    this.pingpongModulator.xBoundary = options.world.width;
    this.pingpongModulator.yBoundary = options.world.height/2;
   
    var pingModData;
    this.delayModPoll = new Tone.Loop((time) => {

      var delayModData = this.pingpongModulator.data();
      
      this.pingpongDelay.feedback.value = delayModData[0];
      //console.log("Feedback: " + delayModData[0]);
      this.pingpongDelay.delayTime.rampTo = (Math.round(delayModData[1] * 16)) * Tone.Time("16n");
      //console.log("pingpongDelay time: " + (Math.round(delayModData[1] * 16)) * Tone.Time("16n"));
    }, "4n").start(0);
    this.add(options);       
}

 lakaPiece.prototype.add = function (options) {
    
    options.color = setNewColor();
    options.delay = 1;
    options.enviro = this.enviro;
    options.world = this.world;
 
    // Pattern part 1
    this.enviro.chordProgGen.changeProgression("iii vi");
    
    options.x = options.world.width/8 * 2;
    options.y = options.world.height/8 * 6;
    options.interval = "16n";
    this.arp1 = this._createPatterns(options);
    this.arp1.modulator.xOrigin = 0
    this.arp1.modulator.xBoundary = options.world.width/2;
    this.arp1.modulator.yOrigin = options.world.height/2;
    this.arp1.modulator.yBoundary = options.world.height;

    this.arp1.instrument.modulator.xOrigin = 0;
    this.arp1.instrument.modulator.xBoundary = options.world.width/2;
    this.arp1.instrument.modulator.yOrigin = options.world.height/2;
    this.arp1.instrument.modulator.yBoundary = options.world.height;
    this.noteGens.push(this.arp1);

    // Pattern part 2
    this.enviro.chordProgGen.changeProgression("1");
        
    options.x = options.world.width/8 * 6;
    options.y = options.world.height/8 * 6;
    this.arp2 = this._createPatterns(options);
    this.arp2.modulator.xOrigin = options.world.width/2;
    this.arp2.modulator.xBoundary = options.world.width;
    this.arp2.modulator.yOrigin = options.world.height/2;
    this.arp2.modulator.yBoundary = options.world.height;

    this.arp2.instrument.modulator.xOrigin = options.world.width/2;
    this.arp2.instrument.modulator.xBoundary = options.world.width;
    this.arp2.instrument.modulator.yOrigin = options.world.height/2;
    this.arp2.instrument.modulator.yBoundary = options.world.height;
    this.noteGens.push(this.arp2);


    // Kick
    // into top right quadrant
    options.x = options.world.width/8 * 7;
    options.y = options.world.height/8 *2;
    this.kick = this._createKick(options);
    
    this.kick.instrument.modulator.xOrigin = options.world.width/2;
    this.kick.instrument.modulator.xBoundary = options.world.width;
    this.kick.instrument.modulator.yOrigin = 0;
    this.kick.instrument.modulator.yBoundary = options.world.height/2;
    
    this.noteGens.push(this.kick);

    // Perc
    var percOptions = {period: "16n"};
    options.period = "8n";
    options.color = "#a1887f";

    // Putting this in the top left quadrant
    options.x = options.world.width/8;
    options.y = options.world.height/8;
    this.closedhat = this._createPerc(options);
    this.closedhat.modulator.xBoundary = options.world.width/2;
    this.closedhat.modulator.yBoundary = options.world.height/2;

    this.closedhat.instrument.modulator.xBoundary = options.world.width/2;
    this.closedhat.instrument.modulator.yBoundary = options.world.height/2;

    this.noteGens.push(this.closedhat);

    options.color = "#d0704e";
    this.closedhat2 = this._createPerc(options);
    this.closedhat2.modulator.xBoundary = options.world.width/2;
    this.closedhat2.modulator.yBoundary = options.world.height/2;

    this.closedhat2.instrument.modulator.xBoundary = options.world.width/2;
    this.closedhat2.instrument.modulator.yBoundary = options.world.height/2;

    this.closedhat2.instrument.instrument.harmonicity = 9.5;


    this.noteGens.push(this.closedhat2);

    var self = this;
    Tone.Transport.schedule(function(time){
      //arp2.start();
      //closedhat.start();
      //kick.start();
      //bass.start();
      //arp1.start();
      
    }, "1m");
   
    var moveLoop = new Tone.Loop((time) => {
      this.pingpongModulator.moveIt();
      for (var i = 0; i < this.noteGens.length; i++) {
        if (this.noteGens[i].modulator != null) {
          this.noteGens[i].modulator.moveIt();
        }
        
      }
    }, "1m").start(0);

  // bring volume up on everything
  for (var i = 0; i++; i < this.noteGens.length) {
    this.noteGens[i].instrument.channel.volume.rampTo(-36, "2m");
  }
   
 }

lakaPiece.prototype._createPerc = function (options) {
  var simpleBeat = new simpleBeatGenerator(options);
  var percInstrument = new metalSynthInstr(options);
  simpleBeat.instrument = percInstrument;
  options.size = 500;
  var simpleBeatModulator = new ballModulator(options);
  simpleBeat.modulator = simpleBeatModulator;

  simpleBeat.instrument.modulator = new ballSquareModulator(options);

    // Mixing
   simpleBeat.instrument.channel.volume.value = -9;
   simpleBeat.instrument.instrument.envelope.decay = 0.05;
   simpleBeat.instrument.sendVol.connect(this.pingpongDelay);
   simpleBeat.instrument.sendVol.volume.value = -12;

   return simpleBeat;

}

 lakaPiece.prototype._createKick = function (options) {
  var simpleBeat = new simpleBeatGenerator(options);
  var kickInstrument = new membraneSynthInstr();
  simpleBeat.instrument = kickInstrument;
  simpleBeat.instrument.modulationOrder = ["pitchdecay", "vol", "filter", "attack"];
  options.size = 500;
  var kickModulator = new ballSquareModulator(options);
  simpleBeat.instrument.modulator = kickModulator;

  // Mixing
  simpleBeat.instrument.channel.volume.value = -6;
  return simpleBeat;
  
 }
 
 

lakaPiece.prototype._createBeat = function (options) {
  var beatGen = new simpleBeatGenerator(options);
  options.size = 460;
  var beatMod = new ballModulator(options);
  beatGen.modulator = beatMod;
  
  var beatInst = new simplePlayer(options);
  beatGen.instrument = beatInst;
  beatGen.instrument.modulationOrder = ["attack", "pan", "filter", "vol"];
  options.size = 500;
  options.x = options.world.width/8 * 3;
  options.y = options.world.height/8 * 3;
  beatGen.instrument.modulator = new ballModulator(options);

  // FX send connections
  if (options.delay == 1) {
    beatGen.instrument.sendVol.connect(this.pingpongDelay);
  } else beatGen.instrument.sendVol.connect(this.feedbackDelay);
  beatGen.instrument.sendVol.connect(this.reverb);

  // Mixing
  beatGen.instrument.channel.volume.value = -16;
  beatGen.instrument.sendVol.volume.value = -12;

  this.noteGens.push(beatGen);
  
}

// Add pattern generators

lakaPiece.prototype._createPatterns = function (options) {  

    var patternGen = new patternGenerator(options);
    options.size = 240;
    options.color = "#81c784";
    var patternGenMod = new ballSquareModulator(options);
    patternGen.modulator = patternGenMod;
 
    var patternInstrument = new monoSynthInstr(options);
    patternGen.instrument = patternInstrument;
    patternGen.instrument.modulationOrder = ["pan", "vol", "filter", "attack"];
    //patternGen.instrument.volRange = -6;
    patternGen.instrument.volRange = -9;
    patternGen.instrument.volMax = -24;

    options.size = 320;

    options.color = "#00a308";
    patternGen.instrument.modulator = new ballSquareModulator(options);

    // FX send connections
    
    patternGen.instrument.sendVol.connect(this.pingpongDelay);
    patternGen.instrument.sendVol.connect(this.reverb);

    // Mixing
    //patternGen.instrument.channel.volume.value = -32;
    patternGen.instrument.sendVol.volume.value = -18;

    return patternGen;

}

lakaPiece.prototype.dispose = function() {
    // Stop the Tone loops and dispose of them
    this.delayModPoll.stop();
    this.delayModPoll.dispose();
  
    
    this.pingpongDelay.dispose();
    //this.feedbackDelay.dispose();
    this.reverb.dispose();
    
    this.pingpongModulator.dispose();
    //this.delayModulator.dispose();
    
    for (var i = 0; i < this.noteGens.length; i++) {
        this.noteGens[i].dispose();
    }
  // remove nexusUI elements
    kicktoggle.destroy();
    hattoggle.destroy();
    //basstoggle.destroy();
    pattern1toggle.destroy();
    pattern2toggle.destroy();
    arp1Osc.destroy();
    arp2Osc.destroy();
    multislider.destroy();

    var labels = ["slot1label", "slot2label", "slot3label", "slot4label", "slot5label", "slot6label", "slot7label", "slot8label" ];
    for (var i = 0; i < labels.length; i++) {
      document.getElementById(labels[i]).innerHTML = "";
    }
    

}

function setNewColor(){
  var newColor;
  if (colorPointer < colors.length) {
    newColor = colors[colorPointer];
    colorPointer++;
  } else {
    colorPointer = 0;
    newColor = colors[colorPointer];
  }
    return newColor;
}

export default lakaPiece



