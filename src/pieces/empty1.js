/**
 * Empty - useful as a template
 **/

var colorPointer = 0;
var colors = ["#e57373", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7",
                    "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176",
                    "#ffd54f", "#ffb74d", "#ff8a65", "#a1887f", "#e0e0e0", "#90a4ae"];

function emptyOnePiece(options) {
    this.init(options);
}

emptyOnePiece.prototype.init = function(options) {
    this.enviro  = options.enviro;
    this.world = options.world;
    this.noteGens = [];
    var periods = ["2n", "3n", "4n", "6n", "8n", "16n"];  

    options.world.engine.world.gravity.y = 0;
    
    // Delay
    this.pingpongDelay = new Tone.PingPongDelay("8n", 0.5).toDestination();
    this.feedbackDelay = new Tone.FeedbackDelay("16n * 3", 0.5).toDestination();

    // Reverb
    this.reverb = new Tone.Reverb(8).toDestination();
    //Modulate the delay - period and feedback
    options.color = setNewColor();
    options.size = 400;
    this.pingpongModulator = new ballModulator(options);
    options.color = setNewColor();
    this.delayModulator = new ballModulator(options);
    var delayModData, pingModData;
    this.delayModPoll = new Tone.Loop((time) => {
      delayModData = this.delayModulator.data();
      //console.log("Delay mod: " + (Math.round(delayModData[1] * 16))* Tone.Time("16n"));
      //console.log("Feedback: " + delayModData[0]);
      this.feedbackDelay.feedback.value = delayModData[0];
      this.feedbackDelay.delayTime.rampTo = (Math.round(delayModData[1] * 16)) * Tone.Time("16n");

      delayModData = this.pingpongModulator.data();
      //console.log("Delay mod: " + (Math.round(delayModData[1] * 16))* Tone.Time("16n"));
      //console.log("Feedback: " + delayModData[0]);
      this.pingpongDelay.feedback.value = delayModData[0];
      this.pingpongDelay.delayTime.rampTo = (Math.round(delayModData[1] * 16)) * Tone.Time("16n");


    }, "4n").start(0); 
    
    
}

 emptyOnePiece.prototype.add = function (options) {
    console.log("going to add something");

     //options.samples = samples.sets[sampleKeys[Nexus.ri(0, sampleKeys.length-1)]];
  //options.samples = samples.sets.snare1;
    options.color = setNewColor();
    options.delay = 1;
    options.enviro = this.enviro;
    options.world = this.world;
    if (options.samples[0].pitch == "") {
      options.period = periods[Nexus.ri(0, periods.length -1)];
      options.sample = options.samples[0];
      
      this._createBeat(options, 1);
    } else {
      this._createPatterns(options, Nexus.ri(1,1));
    }

    this.noteGens[i].instrument.channel.volume.rampTo(-12,"2m");
 }


emptyOnePiece.prototype._createBeat = function (options) {
  var beatGen = new simpleBeatGenerator(options);
  options.size = 460;
  var beatMod = new ballModulator(options);
  beatGen.modulator = beatMod;
  
  var beatInst = new simplePlayer(options);
  beatGen.instrument = beatInst;
  beatGen.instrument.modulationOrder = ["attack", "pan", "filter", "vol"];
  options.size = 500;
  beatGen.instrument.modulator = new ballModulator(options);
  // FX send connections
  if (options.delay == 1) {
    beatGen.instrument.sendVol.connect(this.pingpongDelay);
  } else beatGen.instrument.sendVol.connect(this.feedbackDelay);
  beatGen.instrument.sendVol.connect(this.reverb);

  // Mixing
  beatGen.instrument.channel.volume.value = -16;
  beatGen.instrument.sendVol.volume.value = -12;

  this.noteGens.push(beatGen);
  
}

// Add pattern generators

emptyOnePiece.prototype._createPatterns = function (options) {
  

    var patternGen = new patternGenerator(options);
    options.size = 240;
    var patternGenMod = new ballSquareModulator(options);
    patternGen.modulator = patternGenMod;
 
    var patternInstrument = new pitchedSampler(options);
    patternGen.instrument = patternInstrument;
    patternGen.instrument.modulationOrder = ["attack", "pan", "filter", "vol"];

    options.size = 320;
    patternGen.instrument.modulator = new ballModulator(options);

    // FX send connections
    if (options.delay == 1) {
      patternGen.instrument.sendVol.connect(this.pingpongDelay);
    } else patternGen.instrument.sendVol.connect(this.feedbackDelay);
    patternGen.instrument.sendVol.connect(this.reverb);

    // Mixing
    patternGen.instrument.channel.volume.value = -16;
    patternGen.instrument.sendVol.volume.value = -12;

    this.noteGens.push(patternGen);
  

}

emptyOnePiece.prototype.dispose = function() {
    // Stop the Tone loops and dispose of them
    this.delayModPoll.stop();
    this.delayModPoll.dispose();
  
    
    this.pingpongDelay.dispose();
    this.feedbackDelay.dispose();
    this.reverb.dispose();
    
    this.pingpongModulator.dispose();
    this.delayModulator.dispose();
    
    for (var i = 0; i < this.noteGens.length; i++) {
        this.noteGens[i].dispose();
    }
    
}

function setNewColor(){
  var newColor;
  if (colorPointer < colors.length) {
    newColor = colors[colorPointer];
    colorPointer++;
  } else {
    colorPointer = 0;
    newColor = colors[colorPointer];
  }
    return newColor;
}




