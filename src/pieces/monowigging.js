/**
 * MonoWiggingPiece - a piece with a simple Mono Tone Synth to help
 * build pieces in parallel with Bitwig; so build the stuff in Bitwig, and code it
 **/

var colorPointer = 0;
var colors = ["#e57373", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7",
                    "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176",
                    "#ffd54f", "#ffb74d", "#ff8a65", "#a1887f", "#e0e0e0", "#90a4ae"];

function monoWiggingPiece(options) {
    this.init(options);
}

monoWiggingPiece.prototype.init = function(options) {
    this.enviro  = options.enviro;
    this.world = options.world;
    this.noteGens = [];
    this.periods = ["2n", "3n", "4n", "6n", "8n", "16n"];  

    options.world.engine.world.gravity.y = 0;
    
    // Delay
    this.pingpongDelay = new Tone.PingPongDelay("8n", 0.5).toDestination();
    this.feedbackDelay = new Tone.FeedbackDelay("16n * 3", 0.5).toDestination();

    // Reverb
    this.reverb = new Tone.Reverb(8).toDestination();
    //Modulate the delay - period and feedback
    options.color = "#ffb74d";
    options.size = 400;
    this.pingpongModulator = new ballModulator(options);
    options.color = "#ff8a65";
    this.delayModulator = new ballModulator(options);
    var delayModData, pingModData;
    this.delayModPoll = new Tone.Loop((time) => {
      delayModData = this.delayModulator.data();
      
      this.feedbackDelay.feedback.value = delayModData[0];
      this.feedbackDelay.delayTime.rampTo = (Math.round(delayModData[1] * 16)) * Tone.Time("16n");

      delayModData = this.pingpongModulator.data(); //FIXME better named getData?
      
      this.pingpongDelay.feedback.value = delayModData[0];
      this.pingpongDelay.delayTime.rampTo = (Math.round(delayModData[1] * 16)) * Tone.Time("16n");


    }, "4n").start(0);
  
    this.add(options);
   
}

 monoWiggingPiece.prototype.add = function (options) {
    // console.log("going to add something");

    options.color = setNewColor();
    options.delay = 1;
    options.enviro = this.enviro;
    options.world = this.world;
  
    this._createPatterns(options, Nexus.ri(1,1));
    
    this.noteGens[0].instrument.channel.volume.rampTo(-18,"2m");
 }


// Add pattern generators

monoWiggingPiece.prototype._createPatterns = function (options) {
  

    var patternGen = new patternGenerator(options);
    options.size = 240;
    var patternGenMod = new ballSquareModulator(options);
    patternGen.modulator = patternGenMod;
 
    var patternInstrument = new monoSynthInstr(options);
  
  
    patternGen.instrument = patternInstrument;
    patternGen.instrument.modulationOrder = ["attack", "pan", "filter", "vol"];

    options.size = 320;
    patternGen.instrument.modulator = new ballModulator(options);

    // FX send connections
    if (options.delay == 1) {
      patternGen.instrument.sendVol.connect(this.pingpongDelay);
    } else patternGen.instrument.sendVol.connect(this.feedbackDelay);
    patternGen.instrument.sendVol.connect(this.reverb);

    // Mixing
    patternGen.instrument.channel.volume.value = -20;
    patternGen.instrument.sendVol.volume.value = -18;

    this.noteGens.push(patternGen);
  

}

monoWiggingPiece.prototype.dispose = function() {
    // Stop the Tone loops and dispose of them
    this.delayModPoll.stop();
    this.delayModPoll.dispose();
  
    
    this.pingpongDelay.dispose();
    this.feedbackDelay.dispose();
    this.reverb.dispose();
    
    this.pingpongModulator.dispose();
    this.delayModulator.dispose();
    
    for (var i = 0; i < this.noteGens.length; i++) {
        this.noteGens[i].dispose();
    }
    
}

function setNewColor(){
  var newColor;
  if (colorPointer < colors.length) {
    newColor = colors[colorPointer];
    colorPointer++;
  } else {
    colorPointer = 0;
    newColor = colors[colorPointer];
  }
    return newColor;
}




