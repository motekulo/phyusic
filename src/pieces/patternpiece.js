/**
 * Pattern piece - a piece; standalone.
 **/

import * as Tone from 'tone'
import * as Nexus from 'nexusui'
import ballModulator from '../modulators/ball_modulator'
import simpleBeatGenerator from '../note_generators/simplebeat'

var colorPointer = 0;
var colors = ["#e57373", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7",
                    "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176",
                    "#ffd54f", "#ffb74d", "#ff8a65", "#a1887f", "#e0e0e0", "#90a4ae"];

function patternPiece(options) {
    this.init(options);
}

patternPiece.prototype.init = function(options) {
    this.noteGens = [];
    var periods = ["2n", "3n", "4n", "6n", "8n", "16n"];  

    options.world.engine.world.gravity.y = 0;
    
    // Delay
    this.pingpongDelay = new Tone.PingPongDelay("8n", 0.5).toDestination();
    this.feedbackDelay = new Tone.FeedbackDelay("16n * 3", 0.5).toDestination();

    // Reverb
    this.reverb = new Tone.Reverb(8).toDestination();
    //Modulate the delay - period and feedback
    options.color = setNewColor();
    options.size = 400;
    this.pingpongModulator = new ballModulator(options);
    options.color = setNewColor();
    this.delayModulator = new ballModulator(options);
    var delayModData, pingModData;
    this.delayModPoll = new Tone.Loop((time) => {
      delayModData = this.delayModulator.data();
      //console.log("Delay mod: " + (Math.round(delayModData[1] * 16))* Tone.Time("16n"));
      //console.log("Feedback: " + delayModData[0]);
      this.feedbackDelay.feedback.value = delayModData[0];
      this.feedbackDelay.delayTime.rampTo = (Math.round(delayModData[1] * 16)) * Tone.Time("16n");

      delayModData = this.pingpongModulator.data();
      //console.log("Delay mod: " + (Math.round(delayModData[1] * 16))* Tone.Time("16n"));
      //console.log("Feedback: " + delayModData[0]);
      this.pingpongDelay.feedback.value = delayModData[0];
      this.pingpongDelay.delayTime.rampTo = (Math.round(delayModData[1] * 16)) * Tone.Time("16n");


    }, "4n").start(0); 

    // Fade out in the last bar
    this.fadeOut = new Tone.Loop((time) => {

      for (var i = 0; i < this.noteGens.length; i++) {
        this.noteGens[i].instrument.channel.volume.rampTo(-48,"2m");
      }
      console.log("Fading " + Tone.Transport.position);

    }, "8m").start("6m");
  
    // Merge between different sections
    this.changeSections = new Tone.Loop((time) => {

      for (var i = 0; i < this.noteGens.length; i++) {
        this.noteGens[i].dispose();
      }

      Tone.Transport.bpm.value +- Nexus.ri(-10,10);
      //musicEnviro.tonalEnv.setKey(Nexus.ri(0,11));
      //proggies[Nexus.ri(0,6)];

      //Patterns  
      for (var i = 0; i < Nexus.ri(2, 12); i++) {
        options.samples = samples.sets[sampleKeys[Nexus.ri(0, sampleKeys.length-1)]];
        //options.samples = samples.sets.snare1;
        options.color = setNewColor();
        options.delay = 1;
        if (options.samples[0].pitch == "") {
          options.period = periods[Nexus.ri(0, periods.length -1)];
          options.sample = options.samples[0];
          this._createBeat(options, 1);
        } else {
          this._createPatterns(options, Nexus.ri(1,1));   
        }
      }

      for (var i = 0; i < this.noteGens.length; i++) {
          this.noteGens[i].instrument.channel.volume.rampTo(-12,"2m");
      }

      //console.log("Creating " + Tone.Transport.position);
      //console.log("Tempo: " + Tone.Transport.bpm)

    }, "32m").start(0);
}


patternPiece.prototype._createBeat = function (options, number) {
  var beatGen = new simpleBeatGenerator(options);
  options.size = 460;
  var beatMod = new ballModulator(options);
  beatGen.modulator = beatMod;
  
  var beatInst = new simplePlayer(options);
  beatGen.instrument = beatInst;
  beatGen.instrument.modulationOrder = ["attack", "pan", "filter", "vol"];
  options.size = 500;
  beatGen.instrument.modulator = new ballModulator(options);
  // FX send connections
  if (options.delay == 1) {
    beatGen.instrument.sendVol.connect(this.pingpongDelay);
  } else beatGen.instrument.sendVol.connect(this.feedbackDelay);
  beatGen.instrument.sendVol.connect(this.reverb);

  // Mixing
  beatGen.instrument.channel.volume.value = -16;
  beatGen.instrument.sendVol.volume.value = -12;

  this.noteGens.push(beatGen);
  
}

// Add pattern generators

patternPiece.prototype._createPatterns = function (options, number) {
  for (var i = 0; i < number; i++) {

    var patternGen = new patternGenerator(options);
    options.size = 240;
    var patternGenMod = new ballSquareModulator(options);
    patternGen.modulator = patternGenMod;
 
    var patternInstrument = new pitchedSampler(options);
    patternGen.instrument = patternInstrument;
    patternGen.instrument.modulationOrder = ["attack", "pan", "filter", "vol"];

    options.size = 320;
    patternGen.instrument.modulator = new ballModulator(options);

    // FX send connections
    if (options.delay == 1) {
      patternGen.instrument.sendVol.connect(this.pingpongDelay);
    } else patternGen.instrument.sendVol.connect(this.feedbackDelay);
    patternGen.instrument.sendVol.connect(this.reverb);

    // Mixing
    patternGen.instrument.channel.volume.value = -16;
    patternGen.instrument.sendVol.volume.value = -12;

    this.noteGens.push(patternGen);
  }

}

patternPiece.prototype.dispose = function() {
    // Stop the Tone loops and dispose of them
    this.delayModPoll.stop();
    this.delayModPoll.dispose();
    
    this.fadeOut.stop();
    this.fadeOut.dispose();
    
    this.changeSections.stop();
    this.changeSections.dispose();
    
    this.pingpongDelay.dispose();
    this.feedbackDelay.dispose();
    this.reverb.dispose();
    
    this.pingpongModulator.dispose();
    this.delayModulator.dispose();
    
    for (var i = 0; i < this.noteGens.length; i++) {
        this.noteGens[i].dispose();
    }
    
}

function setNewColor(){
  var newColor;
  if (colorPointer < colors.length) {
    newColor = colors[colorPointer];
    colorPointer++;
  } else {
    colorPointer = 0;
    newColor = colors[colorPointer];
  }
    return newColor;
}

export default patternPiece

