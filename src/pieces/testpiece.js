/**
 * Hazy Jangol
 **/

import * as Tone from 'tone';
import * as Nexus from 'nexusui';
import ballModulator from '../modulators/ball_modulator';
import ballSquareModulator from '../modulators/ball_square_modulator';
import simplePartGenerator from '../note_generators/simple_part';
import patternGenerator from '../note_generators/pattern';
import thueMorseGenerator from '../note_generators/thue_morse';
import monoSynthInstr from '../instruments/monosynth';
import simpleBeatGenerator from '../note_generators/simplebeat';
import membraneSynthInstr from '../instruments/membranesynth';
import metalSynthInstr from '../instruments/metalsynth';
import matterFactory from '../matterfactory';


var colorPointer = 0;
var colors = ["#e57373", "#ba68c8", "#9575cd", "#7986cb", "#64b5f6", "#4fc3f7",
                    "#4dd0e1", "#4db6ac", "#81c784", "#aed581", "#dce775", "#fff176",
                    "#ffd54f", "#ffb74d", "#ff8a65", "#a1887f", "#e0e0e0", "#90a4ae"];
var kicktoggle = [];
var pattern1toggle = [];
var arp1Osc = [];


function testPiece(options) {
    this.init(options);
}

testPiece.prototype.init = function(options) {
    this.enviro  = options.enviro;
    this.world = options.world;
    this.factory = new matterFactory();
    this.noteGens = [];
    this.kick = "";
    //this.closedhat = "";
    this.arp1 = "";
    //this.arp2 = "";
    //this.bass = "";
    var periods = ["2n", "3n", "4n", "6n", "8n", "16n"];  

      // Nexus UI interface toggles specific to piece
      kicktoggle = new Nexus.Toggle('#slot1',{
        'size': [40,20],
        'state': false
      })
      var self = this;
      kicktoggle.on('change',function(v) {
        if (v === true) {
          self.kick.start();
        }
        if (v === false) {
          self.kick.stop();
        }
      });
      document.getElementById("slot1label").innerHTML = "Kick";
  
  
      pattern1toggle = new Nexus.Toggle('#slot4',{
        'size': [40,20],
        'state': false
      })
      //var self = this;
      pattern1toggle.on('change',function(v) {
        if (v === true) {
          self.arp1.start();
        }
        if (v === false) {
          self.arp1.stop();
        }
      });
      document.getElementById("slot4label").innerHTML = "Pattern 1";
  
     
  
      arp1Osc = new Nexus.Select('#slot6',{
        'size': [60,30],
        'options': ["sine", "sine24", "square", "sawtooth", "triangle"]
      });
      
      arp1Osc.on('change', function(v) {
        selff.arp1.instrument.instrument.oscillator.type = v.value;
    
      });
      document.getElementById("slot6label").innerHTML = "Pattern 1 Oscillator";
    
    

    options.world.engine.world.gravity.y = 0;
  
    var rectOptions = {
      "x": options.world.width/2,
      "y": options.world.height/2,
      "width": options.world.width,
      "height": 10,
      "slope": 0
    }
    
    // Build the floor/ terrain
    //rectOptions.width = options.world.width * 0.8;
    //rectOptions.x = rectOptions.width/2;
    //options.color = setNewColor();
    rectOptions.color = options.color;
    this.rect1 = this.factory.staticRect(rectOptions);
    options.world.addToWorld(this.rect1);

    rectOptions.width = 10;
    rectOptions.height = options.world.height;
    rectOptions.x = options.world.width/2;
    rectOptions.y = options.world.height/2;
    //rectOptions.y = options.world.height/4;

    this.rect2 = this.factory.staticRect(rectOptions);
    options.world.addToWorld(this.rect2);

    //this.enviro.setChordProg("hazy1");
    
    // Reverb
    this.reverb = new Tone.Reverb(8).toDestination();
  
    // Delay
    this.pingpongDelay = new Tone.PingPongDelay("8n", 0.5).toDestination();
    
    options.color = "#e57373";
    options.size = 400;
    options.x = options.world.width/4;
    options.y = options.world.height/4;
    this.pingpongModulator = new ballModulator(options);
    this.pingpongModulator.xBoundary = options.world.width/2;
    this.pingpongModulator.yBoundary = options.world.height/2;
   
    var pingModData;
    this.delayModPoll = new Tone.Loop((time) => {

      var delayModData = this.pingpongModulator.data();
      
      this.pingpongDelay.feedback.value = delayModData[0];
      //console.log("Feedback: " + delayModData[0]);
      this.pingpongDelay.delayTime.rampTo = (Math.round(delayModData[1] * 16)) * Tone.Time("16n");
      //console.log("pingpongDelay time: " + (Math.round(delayModData[1] * 16)) * Tone.Time("16n"));


    }, "4n").start(0);
    this.add(options);    
    
}

 testPiece.prototype.add = function (options) {
    
    options.color = setNewColor();
    options.delay = 1;
    options.enviro = this.enviro;
    options.world = this.world;
 
    // Pattern part 1
    this.enviro.chordProgGen.changeProgression("1");
    
    options.x = options.world.width/8 * 2;
    options.y = options.world.height/8 * 6;
    options.interval = "4n";
    this.arp1 = this._createPatterns(options);
    this.arp1.modulator.xOrigin = 0
    this.arp1.modulator.xBoundary = options.world.width/2;
    this.arp1.modulator.yOrigin = options.world.height/2;
    this.arp1.modulator.yBoundary = options.world.height;

    this.arp1.instrument.modulator.xOrigin = 0;
    this.arp1.instrument.modulator.xBoundary = options.world.width/2;
    this.arp1.instrument.modulator.yOrigin = options.world.height/2;
    this.arp1.instrument.modulator.yBoundary = options.world.height;
    this.noteGens.push(this.arp1);


    // Kick
    options.period="4n";
    this.kick = this._createKick(options);
    this.noteGens.push(this.kick);

    var self = this;
    Tone.Transport.schedule(function(time){
      //arp2.start();
      //closedhat.start();
      //kick.start();
      //bass.start();
      //arp1.start();
      //time = sample accurate time of the event
    }, "1m");

    Tone.Transport.schedule(function(time){
      //arp1.start();
      //closedhat.start();
      //time = sample accurate time of the event
    }, "5m");

    Tone.Transport.schedule(function(time){
      //kick.start();
      //bass.start();
      //time = sample accurate time of the event
    }, "9m");
   
  // bring volume up on everything
  for (var i = 0; i++; i < this.noteGens.length) {
    this.noteGens[i].instrument.channel.volume.rampTo(-36, "2m");
  }
   
 }

testPiece.prototype._createPerc = function (options) {
  var simpleBeat = new simpleBeatGenerator(options);
  var percInstrument = new metalSynthInstr(options);
  simpleBeat.instrument = percInstrument;
  options.size = 500;
  var simpleBeatModulator = new ballModulator(options);
  simpleBeat.modulator = simpleBeatModulator;

  simpleBeat.instrument.modulator = new ballSquareModulator(options);


    // Mixing
   simpleBeat.instrument.channel.volume.value = -9;
   simpleBeat.instrument.instrument.envelope.decay = 0.05;
   simpleBeat.instrument.sendVol.connect(this.pingpongDelay);
   simpleBeat.instrument.sendVol.volume.value = -12;

   return simpleBeat;

}
 

 testPiece.prototype._createKick = function (options) {
  var simpleBeat = new simpleBeatGenerator(options);

  var kickInstrument = new membraneSynthInstr();
  simpleBeat.instrument = kickInstrument;

  // Mixing
  simpleBeat.instrument.channel.volume.value = -6;
  return simpleBeat;
  
 }
 
 testPiece.prototype._createPart = function (options) {
   var partGen = new simplePartGenerator(options);
   //options.size = 460;
   //var partMod = new ballModulator(options);
   //partGen.modulator = partMod;
   
   var partInstrument = new monoSynthInstr(options);
   partGen.instrument = partInstrument;
   
   partGen.instrument.modulationOrder = ["pan", "vol", "filter", "attack"];

   options.size = 320;
   options.color = "#ba68c8";
   partGen.instrument.modulator = new ballSquareModulator(options);
   //partGen.instrument.modulator.

    partGen.instrument.volRange = -3;
    partGen.instrument.volMax = -12;

    // FX send connections
   
    partGen.instrument.sendVol.connect(this.pingpongDelay);
    partGen.instrument.sendVol.connect(this.reverb);

    // Mixing
    partGen.instrument.channel.volume.value = -12;
    //partGen.instrument.sendVol.volume.value = -18;

    return partGen;

}

testPiece.prototype._createBeat = function (options) {
  var beatGen = new simpleBeatGenerator(options);
  options.size = 460;
  var beatMod = new ballModulator(options);
  beatGen.modulator = beatMod;
  
  var beatInst = new simplePlayer(options);
  beatGen.instrument = beatInst;
  beatGen.instrument.modulationOrder = ["attack", "pan", "filter", "vol"];
  options.size = 500;
  options.x = options.world.width/8 * 3;
  options.y = options.world.height/8 * 3;
  beatGen.instrument.modulator = new ballModulator(options);

  // FX send connections
  if (options.delay == 1) {
    beatGen.instrument.sendVol.connect(this.pingpongDelay);
  } else beatGen.instrument.sendVol.connect(this.feedbackDelay);
  beatGen.instrument.sendVol.connect(this.reverb);

  // Mixing
  beatGen.instrument.channel.volume.value = -16;
  beatGen.instrument.sendVol.volume.value = -12;

  this.noteGens.push(beatGen);
  
}

// Add pattern generators

testPiece.prototype._createPatterns = function (options) {  

    var patternGen = new patternGenerator(options);
    options.size = 240;
    options.color = "#81c784";
    var patternGenMod = new ballSquareModulator(options);
    patternGen.modulator = patternGenMod;
 
    var patternInstrument = new monoSynthInstr(options);
    patternGen.instrument = patternInstrument;
    patternGen.instrument.modulationOrder = ["pan", "vol", "filter", "attack"];
    //patternGen.instrument.volRange = -6;
    patternGen.instrument.volRange = -9;
    patternGen.instrument.volMax = -24;

    options.size = 320;

    options.color = "#00a308";
    patternGen.instrument.modulator = new ballSquareModulator(options);

    // FX send connections
    
    //patternGen.instrument.sendVol.connect(this.pingpongDelay);
    //patternGen.instrument.sendVol.connect(this.reverb);

    // Mixing
    //patternGen.instrument.channel.volume.value = -32;
    patternGen.instrument.sendVol.volume.value = -18;

    return patternGen;

}

testPiece.prototype._createThueMorse = function(options) {
  var thuemorseGen = new thueMorseGenerator(options);
  options.size = 360;
  options.color = "#e0e0e0";
  var thuemorseGenMod = new ballModulator(options);
  thuemorseGen.modulator = thuemorseGenMod;

  var thuemorseInstrument = new monoSynthInstr(options);
  thuemorseGen.instrument = thuemorseInstrument;

  thuemorseGen.instrument.panRange = 0.5;
  thuemorseGen.instrument.volMax = -12;
  thuemorseGen.instrument.volRange = -12;
  thuemorseGen.instrument.filterLow = 100;

  options.color = "#69b0c3";
  thuemorseGen.instrument.modulator = new ballSquareModulator(options);
  // FX send connections
    
  //thuemorseGen.instrument.sendVol.connect(this.pingpongDelay);
  //thuemorseGen.instrument.sendVol.connect(this.reverb);

  // Mixing
  thuemorseGen.instrument.channel.volume.value = -16;
  thuemorseGen.instrument.sendVol.volume.value = -12;

  return thuemorseGen;

}

testPiece.prototype.dispose = function() {
    // Stop the Tone loops and dispose of them
    this.delayModPoll.stop();
    this.delayModPoll.dispose();
  
    
    this.pingpongDelay.dispose();
    //this.feedbackDelay.dispose();
    this.reverb.dispose();
    
    this.pingpongModulator.dispose();
    //this.delayModulator.dispose();
    
    for (var i = 0; i < this.noteGens.length; i++) {
        this.noteGens[i].dispose();
    }
    
    kicktoggle.destroy();
    pattern1toggle.destroy();
    arp1Osc.destroy();

    var labels = ["slot1label", "slot2label", "slot3label", "slot4label", "slot5label", "slot6label", "slot7label", "slot8label" ];
    for (var i = 0; i < labels.length; i++) {
      document.getElementById(labels[i]).innerHTML = "";
    }

}

function setNewColor(){
  var newColor;
  if (colorPointer < colors.length) {
    newColor = colors[colorPointer];
    colorPointer++;
  } else {
    colorPointer = 0;
    newColor = colors[colorPointer];
  }
    return newColor;
}

export default testPiece



