/** @class
 * A simple arpeggiated drone note generator
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the player
 * options.pattern -  "up", "down", "upDown", "downUp" | "alternateUp" |
 * "alternateDown" | "random" | "randomOnce" | "randomWalk"
 **/

 function simpleDroneGenerator(options) {
   this.init(options);
 }
 var patterns = ["up", "down", "upDown", "downUp", "alternateUp",
                 "alternateDown", "random", "randomOnce", "randomWalk"];

 simpleDroneGenerator.prototype.init = function(options) {
   this.enviro = options.enviro; // main array for pitch data
   this.world = options.world;
  // this.samples = options.samples;  // array
   this.label = options.label;
   //this.color = options.color;
   this.pitchedSampleLoaded = false;
   this.chordToneSeq = [];
   this.rhythmSeq = [];
   this.intervalDurations = ["1m", "2n", "1.5n", "4n"];
   this.octaveRange = 3;
   this.instrument = null;
   this.modulator = null;
   var value = null;
   this.dronePattern = new Tone.Pattern((time, note) => {
     //console.log("Pattern time:" + time + " note" + note);
     value = {note: note};
     this.instrument.play(value, time);
     //this.droneSynth.triggerAttackRelease(note, "2m", time);
   }, [], options.pattern);
   this.dronePattern.interval = "2m";
   this.setPattern(4, -1);
   this.dronePattern.start(0);

   this.modulationPoll = new Tone.Loop((time) => {
     if (this.modulator != null) {
       modulationData = this.modulator.data();
       if (modulationData[0] != null) {
         var x = Math.floor(modulationData[0] * patterns.length);
         //console.log("Poll for pattern index is  " + x);
         this.dronePattern.pattern = patterns[x];//this.changeRhythm(modulationData[0]);
       };
       if (modulationData[1] != null) {
         var y = Math.floor(modulationData[1] * this.octaveRange); // 7 is number of octaves to choose from
         //console.log("Poll for octave index is  " + y);
         this.setPattern(8, y);
       };

       if (modulationData[2] != null) {
         var z = Math.floor(modulationData[2] * this.intervalDurations.length); // 7 is number of octaves to choose from
         //console.log("Poll for interval index is  " + z);
         this.dronePattern.interval = this.intervalDurations[z];
       };

       if (modulationData[3] != null) {
         //var z = Math.floor(modulationData[2] * this.intervalDurations.length); // 7 is number of octaves to choose from
         //console.log("Poll for probability index is  " + modulationData[3]);
         this.dronePattern.probability = modulationData[3];
       };

     }
     
     this.setPattern(8, 0);  // change chord or key FIXME - why 8?
   }, "1m").start(Tone.Time("0:3").toSeconds());

   this.moveIt = new Tone.Loop((time) => {
     //console.log("Move it");
     if (this.modulator != null) {
       for (var i = 0; i < this.modulator.composite.bodies.length; i++) {
         var x = this.modulator.composite.bodies[i].velocity.x;
         var y = this.modulator.composite.bodies[i].velocity.y;
         Matter.Body.setVelocity(this.modulator.composite.bodies[i], {x: x * 1.2, y: y * 1.2});
       }
     }

   }, "1m").start();

 }

 /**
  * Set the note array (values) of the pattern
  * @param {int} length - the number of notes in the pattern
  * @param {int} octave - the number of the base octave for the notes
  **/
 simpleDroneGenerator.prototype.setPattern = function(length, octave) {

     var p = [];
     var note = "";
     for (var i = 0; i < length; i++) {
         note = Tone.Frequency(this.enviro.pitches[i] + (octave * 12), "midi").toNote();
         p.push(note);
     }
     //console.log("set pattern to " + p);
     this.dronePattern.set({
       values: p
     })
 }

 simpleDroneGenerator.prototype.dispose = function() {
   this.dronePattern.dispose();
   this.instrument.dispose();
   if (this.modulator != null) {
     this.modulator.dispose();
   }

 }
