/** @class
 * A simple arpeggiated drone note generator
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the player
 * options.pattern -  "up", "down", "upDown", "downUp" | "alternateUp" |
 * "alternateDown" | "random" | "randomOnce" | "randomWalk"
 **/

import * as Tone from 'tone'

 function patternGenerator(options) {
   this.init(options);
 }
//var patterns = ["up", "down", "upDown", "downUp", "alternateUp",
  //               "alternateDown", "random", "randomOnce", "randomWalk"];
var patterns = ["up", "down", "alternateUp", "random"];

patternGenerator.prototype.init = function(options) {
  this.enviro = options.enviro; // main array for pitch data
  this.world = options.world;
  // this.samples = options.samples;  // array
  this.label = options.label;
   //this.color = options.color;
  this.pitchedSampleLoaded = false;

  this.intervalDurations = ["2n", "4n", "8n", "16n"];
   this.modulationOrder = ["pattern", "range", "duration", "probability"];
  this.octaveRange = 3;
  this.instrument = null;
  this.modulator = null;
  var value = null;
  this.pattern = new Tone.Pattern((time, note) => {
     //console.log("Pattern time:" + time + " note" + note);
     value = {note: note};
     this.instrument.play(value, time);

    }, ["c1", "c2", "c3", "c1", "c2", "c3", "c1", "c2"], "up");
    this.pattern.set({interval: options.interval});
    this.setChordPattern(8, 0);
    this.pattern.start(0);
    this.pattern.mute = true;

      // Poll periodically for modulation values
    const modulationPoll = new Tone.Loop((time) => {
	
      if (this.modulator != null) {
        var modulationData = this.modulator.data();
        for (var i = 0; i < modulationData.length; i++) {
            this.mapModulation(this.modulationOrder[i], modulationData[i]);
            }
      }
      //console.log("Pattern progress " + this.pattern.progress);

   }, "1m").start(0); // polling every quarter note here
   
}

patternGenerator.prototype.start = function() {
  //this.pattern.start(0);
  this.pattern.mute = false;
}

patternGenerator.prototype.stop = function() {
  //this.pattern.stop();
  this.pattern.mute = true;
}

 /**
  * Set the note array (values) of the pattern
  * @param {int} length - the number of notes in the pattern
  * @param {int} octave - the number of the base octave for the notes
  *
  * So have a pattern for four bars; Then set a loop that at the end of the previous bar, 
  * sets the notes of the pattern for tthe subsequent bar?
  **/
 patternGenerator.prototype.setChordPattern = function(length, octave) {

     var p = [];
     var note = "";
     for (var i = 0; i < length; i++) {
          note = Tone.Frequency(this.enviro.pitches[i] + (octave * 12), "midi").toNote();
          p.push(note);
          //p[i] = note;
     }
     //console.log("set pattern to " + p);
     this.pattern.set({
        values: p
     })
     console.log(this.pattern.values);
 }
 
 
 patternGenerator.prototype.mapModulation = function(modulationType, data) {
  switch(modulationType) {
    case "pattern": 
      var x = Math.floor(data * patterns.length);
      //console.log("Poll for pattern index is  " + x);
      this.pattern.pattern = patterns[x]; // FIXME - should be set
      break;
      
    case "range":
      var y = Math.floor(data * 6); 
      this.setChordPattern(8, y);
      console.log("y for range is " + y);
      break;
    
    case "duration":
      var z = Math.floor(data * 4) + 1; 
      //console.log("Rate is " + z);
      //this.pattern.playbackRate = z;
      //this.pattern.interval = this.intervalDurations[z];
      //this.pattern.set({interval: this.intervalDurations[z]})
      
      //console.log(this.pattern.interval);
      break;
      
    case "probability":
      this.pattern.probability = data;
      // console.log("Probability " + data);
      
  }
} 
 

 patternGenerator.prototype.dispose = function() {
   this.pattern.dispose();
   this.instrument.dispose();
   if (this.modulator != null) {
     this.modulator.dispose();
   }

 }

 export default patternGenerator

