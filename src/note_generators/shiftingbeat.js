/** @class
 * A simple beat generator that responds to the following modulation:
 1. The probability that a note event will fire

 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the player
 * options.period - the frequency (in Tone js note values) of the loop
 * options.note - the pitch of the note to play
 *
 **/

 function shiftingBeatGenerator(options) {
   this.init(options);
 }

 shiftingBeatGenerator.prototype.init = function(options) {
   this.enviro = options.enviro; // main array for pitch data
   this.world = options.world;
   this.label = options.label;
   this.color = options.color;
   this.period = options.period;
   this.note = options.note;
   this.modulator = null;
   this.instrument = null;
   //this.rates = []

   this.beatLoop = new Tone.Loop((time) => {
     if (this.instrument != null) {
        this.instrument.play(options, time);
     };
   }, this.period).start(0);

   this.modulationPoll = new Tone.Loop((time) => {
     if (this.modulator != null) {
       modulationData = this.modulator.data();
       if (modulationData[0] != null) {
         this.beatLoop.probability = modulationData[0];
       };
       if (modulationData[1] != null) {
           this.beatLoop.playbackRate = Math.floor(modulationData[1] * 4) + 1;
       };

     }
     //console.log("Pattern values " + this.dronePattern.values);
   }, "1m").start(Tone.Time("0:3").toSeconds());

 }


 shiftingBeatGenerator.prototype.dispose = function() {
   this.beatLoop.dispose();
   this.instrument.dispose();
   if (this.modulator != null) {
     this.modulator.dispose();
   }

 }
