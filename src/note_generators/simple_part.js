/** @class
 * A simple part player that plays a part based on scale degrees
 * and any associated alterations. Designed to not change as chords
 * change. It will transpose if the key or scale type changes, however.
 *
 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the player
 * options.sequence - note sequence to be played
 * options.octave - octave that the part is based in
 *
 **/

import * as Tone from 'tone'

function simplePartGenerator(options) {
    this.init(options);
 }


simplePartGenerator.prototype.init = function(options) {
    this.enviro = options.enviro; // main array for pitch data
    this.world = options.world;
  // this.samples = options.samples;  // array
    this.label = options.label;
   //this.color = options.color;
    this.pitchedSampleLoaded = false;
//   this.chordToneSeq = [];
//   this.rhythmSeq = [];

    this.instrument = null;
    this.modulator = null;

    this.simplePart = new Tone.Part((function(time, note) {
    this.instrument.play(note, time);
       }).bind(this), []);

    this.setNotes(options.sequence, options.octave);

    this.modulationPoll = new Tone.Loop((time) => {  
     if (this.modulator != null) {
       modulationData = this.modulator.data();
       if (modulationData[0] != null) {
         //var x = Math.floor(modulationData[0] * patterns.length);
         //console.log("Poll for pattern index is  " + x);
         //this.dronePattern.pattern = patterns[x];//this.changeRhythm(modulationData[0]);
       };
       if (modulationData[1] != null) {
         //var y = Math.floor(modulationData[1] * this.octaveRange); // 7 is number of octaves to choose from
         //console.log("Poll for octave index is  " + y);
         //this.setPattern(8, y);
       };

       if (modulationData[2] != null) {
         //var z = Math.floor(modulationData[2] * this.intervalDurations.length); // 7 is number of octaves to choose from
         //console.log("Poll for interval index is  " + z);
         //this.dronePattern.interval = this.intervalDurations[z];
       };

       if (modulationData[3] != null) {

         //this.dronePattern.probability = modulationData[3];
       };

     }
     // reset the notes in case transposition needed
     this.setNotes(options.sequence, options.octave);
     //console.log("Pattern values " + this.dronePattern.values);
   }, "1m").start(Tone.Time("0:3").toSeconds());

    this.moveIt = new Tone.Loop((time) => {
     //console.log("Move it");
     if (this.modulator != null) {
       for (var i = 0; i < this.modulator.composite.bodies.length; i++) {
         var x = this.modulator.composite.bodies[i].velocity.x;
         var y = this.modulator.composite.bodies[i].velocity.y;
         Matter.Body.setVelocity(this.modulator.composite.bodies[i], {x: x * 1.2, y: y * 1.2});
       }
     }

   }, "1m").start();

}

simplePartGenerator.prototype.start = function() {
  this.simplePart.start();
}

simplePartGenerator.prototype.stop = function() {
  this.simplePart.stop();
}

simplePartGenerator.prototype.setNotes = function(notes, octave) {
  // This would be two stages; take the notes as numbers in relation to
  // a tonic (with associated altered notes array?) then convert that
  // notes through the musicenvironment and then add to the part
  var fullScale = this.enviro.getFullScale();
  var patternNotes = [];
  var midinote = 0;
  var note = 0;
  this.simplePart.clear();
  notes.forEach(element => {
    
    midinote = fullScale[(octave * 7 - 1) + element.degree] + element.alt;
    //patternNotes.push((fullScale[(octave * 7 - 1) + element.note]) + element.alt);
    note = Tone.Frequency(midinote, "midi").toNote();
    element.note = note;
    //convertedElement.note = note;
    this.simplePart.at(element.time, element);
    
  });
  
  // Now adjust those value for any altered scale tones:
  
  
  //notes.forEach(element => this.simplePart.at(element.time, element));
}

simplePartGenerator.prototype.dispose = function() {
  this.simplePart.dispose();
  
  this.instrument.dispose();
  if (this.modulator != null) {
    this.modulator.dispose();
  }

}

export default simplePartGenerator




