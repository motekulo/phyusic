/** @class
* A note generator based on the Thue Morse Sequence
* @param {object} options
* @example
* options.enviro - a musicEnvironment
* options.world -a matterWorld
* options.label - a text label for the player
* options.color - in the format '#1e88e5'
**/

import * as Tone from 'tone'

function thueMorseGenerator(options) {   // must pass in a body - that's not an option
    this.init(options);
}

/**
* Intitialize player with given options
**/
thueMorseGenerator.prototype.init = function(options) {

    this.enviro = options.enviro; // main array for pitch data
    this.world = options.world;
  //  this.samples = options.samples;  // array
    this.label = options.label;
    this.color = options.color;
  //  this.pitchedSampleLoaded = false;
    this.chordToneSeq = [];
    this.rhythmSeq = [];
    this.instrument = null;
    this.modulator = null;
    this.octaveAdjust = 0;

    this.generateSeq(8);
    this.mapSeqToChordTones(7, 0, 16);

    this.rhythmSeriesOffset = 0;
    this.mapSeqToRhythm(this.rhythmSeriesOffset, 16);
    var value = null;
    this.thueMorsePart = new Tone.Part((function(time, note) {
        value = {note: note};
        this.instrument.play(value, time);
    }).bind(this),[]);
    this.thueMorsePart.loop = true;
    this.thueMorsePart.loopEnd = "1m";  // fixme
    
    this.mapSeqToPitches(8);

    this.modulationPoll = new Tone.Loop((time) => {
        var modulationData = null;
      if (this.modulator != null) {
        modulationData = this.modulator.data();
        if (modulationData[0] != null) {
          this.changeRhythm(modulationData[0]);
        };
        if (modulationData[1] != null) {
            this.changePitchRange(modulationData[1]);
          };
        //console.log("Poll for mod  " + modulationData);
      }

      //console.log("Thue poll at " + Tone.Transport.position);
      // And use this opportunity to check for a chord change
      this.mapSeqToPitches(8);
    }, "1m").start(Tone.Time("0:3").toSeconds()); //Tone.Time("0:4").toSeconds()

}

thueMorseGenerator.prototype.start = function() {
    this.thueMorsePart.start(0);
}

thueMorseGenerator.prototype.stop = function() {
    this.thueMorsePart.stop(0);
}

/**
 * For modulation - changes the rhythm of the pattern
 * @param {*} offset 
 */
thueMorseGenerator.prototype.changeRhythm = function(offset) {

    this.mapSeqToRhythm(Math.round(offset * 10), 16);
    this.thueMorsePart.clear();
    this.mapSeqToPitches(8);

}

thueMorseGenerator.prototype.changePitchRange = function(range) {
   
    this.octaveAdjust = Math.floor(range * 4);
    //console.log("Octave adjust is " + this.octaveAdjust);

} 


/**
* Update thueMorsePart by mapping this.chordToneSeq and this.pitches
* @param {len} Length - how many sequence items to match
**/
thueMorseGenerator.prototype.mapSeqToPitches = function(len) {

  // chordToneSeq is an array with chordtones (1,3,5, or 7) and we need
  // to map these to actual midi pitches, as held in the musicEnvironment pitches
  // array.

    var sixteenthTally = 0;
    for (var i = 0; i < len; i++) {

        if (this.chordToneSeq[i] === 1) {
            // map root
            this.thueMorsePart.at(sixteenthTally * Tone.Time("16n"),
                Tone.FrequencyClass.mtof(this.enviro.pitches[0] + this.octaveAdjust * 12));
        };
        if (this.chordToneSeq[i] === 3) {
            // map third
            this.thueMorsePart.at(sixteenthTally * Tone.Time("16n"),
                Tone.FrequencyClass.mtof(this.enviro.pitches[1] + this.octaveAdjust * 12));
        };
        if (this.chordToneSeq[i] === 5) {
            // map fifth
            this.thueMorsePart.at(sixteenthTally * Tone.Time("16n"),
                Tone.FrequencyClass.mtof(this.enviro.pitches[2] + this.octaveAdjust * 12));
        };
        if (this.chordToneSeq[i] === 7) {
            // map seventh; if there is no 7th in the array, it'll map to the
            // root so no dramas
            this.thueMorsePart.at(sixteenthTally * Tone.Time("16n"),
                Tone.FrequencyClass.mtof(this.enviro.pitches[3] + this.octaveAdjust * 12));
        };
        sixteenthTally += this.rhythmSeq[i];
    }
    // Make the first beat the root note no matter what:
    this.thueMorsePart.at(0,
        Tone.FrequencyClass.mtof(this.enviro.pitches[0] + this.octaveAdjust * 12));

}

/**
* Generate a Thue Morse sequence for the player
* @param {int} Length of sequence to generate
* @returns {int} Sequence
**/
thueMorseGenerator.prototype.generateSeq = function(len) {
    //var seq = [0];
    this.seq = nthTerm(len);
    //console.log(this.seq);
   //return seq;

    function nthTerm(l) {
        var s = [0];
        for (var i = 1; i < l; i++) {
            var a = [];
            for (var k = 0; k < s.length; k++) {
                if (s[k] === 0) {
                    a[k] = 1;
                } else {
                    a[k] = 0;
                }
            }
            //var compArray = comp(s);
            s = s.concat(a);
        }
        return s;
    }
}

/**
* Maps the Thue Morse Sequence to the chord tone array
* At this stage mapping 0 to 7th, 1 to 5th, 11 to third, 00 to 7th
* @param {int} toChordTone - either 5 or 7
* @param {int} offset - how far into the sequence to start
* @param {int} numToMap - the number of tones to map
**/
thueMorseGenerator.prototype.mapSeqToChordTones = function(toChordTone, offset, numToMap) {
    if (numToMap > this.seq.length) {
        numToMap = this.seq.length - 1;
        offset = 0;
    };
    if (offset + numToMap > this.seq.length) {
        offset = 0;
    };

    var chordToneSeq = [];
    var done = false;
    var i = offset;
    //var k = 0;
    // mapping 0 to 7th, 1 to 5th, 11 to third, 00 to 7th
    while (!done) {
        if (chordToneSeq.length >= numToMap) {done = true};
        if (this.seq[i] === 0 && this.seq[i+1] === 0) {
            chordToneSeq.push(7); // pushing chordtones to array
            i = i + 2;
            continue;
        };
        if (this.seq[i] === 0 && this.seq[i+1] === 1) {
            chordToneSeq.push(1);
            i = i + 1;
            continue;
        };
        if (this.seq[i] === 1 && this.seq[i+1] === 1) {
            chordToneSeq.push(3);
            i = i + 2;
            continue;
        };
        if (this.seq[i] === 1 && this.seq[i+1] === 0) {
            chordToneSeq.push(5);
            i = i + 1;
            continue;
        };
    }
    //console.log(chordToneSeq);
    this.chordToneSeq = chordToneSeq;
}

/**
* Maps the Thue Morse Sequence to the rhythm sequence
* 00 mapped to 1 semiquaver, 0 to 2, 11 to 3, and 1 to 4
* @param {int} offset - how far into the sequence to start
* @param {int} numToMap - the number of tones to map
**/
thueMorseGenerator.prototype.mapSeqToRhythm = function(offset, numToMap) {
    if (numToMap > this.seq.length) {
        numToMap = this.seq.length - 1;
        offset = 0;
    };
    if (offset + numToMap > this.seq.length) {
        offset = 0;
    };

    var rhythmSeq = [];
    var done = false;
    var i = offset;
    //var k = 0;
    // mapping 0 to 7th, 1 to 5th, 11 to third, 00 to 7th
    while (!done) {
        if (rhythmSeq.length >= numToMap) {done = true};
        if (this.seq[i] === 0 && this.seq[i+1] === 0) {
            rhythmSeq.push(1); // number of 16ths, or whatever time value
            i = i + 2;
            continue;
        };
        if (this.seq[i] === 0 && this.seq[i+1] === 1) {
            rhythmSeq.push(2);
            i = i + 1;
            continue;
        };
        if (this.seq[i] === 1 && this.seq[i+1] === 1) {
            rhythmSeq.push(3);
            i = i + 2;
            continue;
        };
        if (this.seq[i] === 1 && this.seq[i+1] === 0) {
            rhythmSeq.push(4);
            i = i + 1;
            continue;
        };
    }
    //console.log(rhythmSeq);
    this.rhythmSeq = rhythmSeq;
}

thueMorseGenerator.prototype.dispose = function() {
  this.thueMorsePart.dispose();
  this.modulationPoll.dispose();
  this.instrument.dispose();
  if (this.modulator != null) {
    this.modulator.dispose();
  }

}

export default thueMorseGenerator
