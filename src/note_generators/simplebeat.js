/** @class
 * A simple beat generator that responds to the following modulation:
 1. The probability that a note event will fire

 * @param {object} options
 * @example
 * options.enviro - a musicEnvironment
 * options.world -a matterWorld
 * options.label - a text label for the player
 * options.period - the frequency (in Tone js note values) of the loop
 * options.note - the pitch of the note to play
 *
 **/

 import * as Tone from 'tone'

 function simpleBeatGenerator(options) {
   this.init(options);
 }

 simpleBeatGenerator.prototype.init = function(options) {
   this.enviro = options.enviro; // main array for pitch data
   this.world = options.world;
   this.label = options.label;
   this.color = options.color;
   this.period = options.period;
   this.note = options.note;
   this.modulator = null;
   this.instrument = null;
   //this.rates = []

   this.beatLoop = new Tone.Loop((time) => {
     if (this.instrument != null) {
        this.instrument.play(options, time);
     };
   }, this.period);//.start(0);



   this.modulationPoll = new Tone.Loop((time) => {
     if (this.modulator != null) {
        var modulationData = this.modulator.data();
        if (modulationData[0] != null) {
         this.beatLoop.probability = modulationData[0];
        };
        // Not really the place for this but sounds good
        if (modulationData[1] != null) {
          this.instrument.sendVol.volume.rampTo(modulationData[1] * -12 - 6, "4n");
        };
        //console.log("sendVol " + this.instrument.sendVol.volume.value );
      };

     
     //console.log("Pattern values " + this.dronePattern.values);
   }, "1m").start(Tone.Time("0:3").toSeconds());

 }

 simpleBeatGenerator.prototype.start = function() {
  this.beatLoop.start();
 }

 simpleBeatGenerator.prototype.stop = function() {
  this.beatLoop.stop();
 }

 simpleBeatGenerator.prototype.dispose = function() {
   this.beatLoop.dispose();
   this.instrument.dispose();
   if (this.modulator != null) {
     this.modulator.dispose();
   }

 }

 export default simpleBeatGenerator
 
