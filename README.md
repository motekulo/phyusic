# Phyusic

Experiments connecting a music library ([Tone js](https://tonejs.github.io/)) with a physics engine ([Matter js](https://brm.io/matter-js/))

## Docs

Use [JSDoc](https://github.com/jsdoc/jsdoc)

So something like:

  jsdoc www/js -r -d tmp


## Design - backend

There is an overall environment (a bit like the rack infrastructure of vcvrack) that takes user input, adds instruments, deletes them, and so on. This can be thought of as the "piece", and it has a Tone.Transport, key, instances of my own tonality environment. That allows for chord changes and progressions. So it has chord progressions, and should broadcast a changed note array every time there is a change?

### Chord Progression Generator

The overall piece has a single chord progression generator. This could take on a number of forms (so there would be a choice) but basically there would be some specs as to what a chord generator would need to do. Probably something as simple as broadcasting to the piece that a chord change had taken place, or is about to take place.

A chord progression generator decides on a chord, then broadcasts that via a callback to the musicenvironment.

### Note generator

A note generator generates notes. This could be via a Tone js pattern or some other way. It should have an instrument object that renders these notes, via the instrument's play method, taking note and time parameters.

A note generator has to have a "dispose" method that removes any Tone js events it might have created (a Tone.Loop or Tone.Part, for example) along with any associated instruments and modulators.

### Instrument

An instrument or at least an object that makes sound in the context of the overall "piece" or environment consists of a sequencer connected to a sound source. That's how to think of it from a user perspective, anyway. Underneath, things are slightly more complicated, in that a sequencer has a part, and that part fires a callback with timing events, in which the particular instrument is then triggered.

In the main piece, we have an array that holds all of the sequences, and each of those will have an object property that is their currently associated instrument.

An instrument can have a modulator, as can a sequencer. A modulator consists of a physics (matter js) aggregate, such as balls that move and bounce, or a Newton's cradle. That modulator can either:

- transmit modulation information on collision
- transmit modulation information at a certain rate

Another possibility might be that an instrument polls a modulator for information? Need to think this relationship through.

An instrument has to have a "play" method, taking note and time parameters, and play that note at that time using Tone js.

An instrument has to have a "dispose" method that disposes of all Tone js elements it has created, along with any associated modulators.

### Modulators

These can be connected to either instruments, or note generators. Good to be able to modulate as much as possible.

* Need to be able to connect a modulator to an effect parameter *


Both instruments and note generators can have a modulator object. The method "data" is called on that object (maybe a better name could be devised - getData?) at whatever rate the owner decides (via a Tone.Loop, for example). The modulator needs to implement that data method, and return an array of modulation data. That array consists of values between 0 and 1. It is up to the instrument or note generator to make sense of that value to modulate particular parameters. The modulator.data() function takes no arguments, and must always return an array - even if empty. It is then up to the owner of the modulator (so the instrument or note generator) to check the length of the returned array and use the data (if any) accordingly.

Modulators have been conceptualised as being visual interface elements based on matter js composites, but there is no reason a modulator couldn't be something else. All it has to do is return an array of data when its data method is called.

Modulators have an instance variable/object called 'composite'. This consists of the matter js objects (as a composite) for the modulator. The consistent naming is important so that the composite's id can be established when we need to remove it from the matter world.

A modulator must have a "dispose" method that removes any composites it has created from the Matter.World.


### Core framework

Phyusic.js is the main file that connects everything together.

#### Interface ideas

How about being able to import a json file that specifies various different note generators, instruments and modulators? Ultimately this will be via an interface, but this might be a good alternative while developing.

#### index.html

Contains place markers for Nexus widgets; javascript files.

#### index.js

From cordova template; should probably be where the app starts.

#### phyusic.js

The main part of the app that connects everything together.

#### phyusicmatter.js

Creates the matter engine, canvas and adds objects to it.

#### matterfactory.js

Helps build more complex composite matter objects

#### musicenvironment.js

Has some documentation, but sets up a tonal musical environment, chord progression, relevant Tone js parts (for a metronome, for example).

Uses tonality.js, chordProggies, sampleSet

## Extending

### Adding new samples

Add new samples to the www/assets/samples directory, under one of the classifying directories ("bass", for example).

Add relevant pitch and location information to the samplesets.js file. A new sample consists of an object that has pitch and location information (see examples in samplesets.js).

In phyusic.js, add a descriptive name to the sampleNames array. Then, in the createInstrument() function, add a case that references the specific sample object name in samplesets.js.


## Color palette

Using the lighten-2 colors from the materialize pallette:

#e57373 red lighten-2
#f06292 pink lighten-2
#ba68c8 purple lighten-2
#9575cd deep-purple lighten-2
#7986cb indigo lighten-2
#64b5f6 blue lighten-2
#4fc3f7 light-blue lighten-2
#4dd0e1 cyan lighten-2
#4db6ac teal lighten-2
#81c784 green lighten-2
#aed581 light-green lighten-2
#dce775 lime lighten-2
#fff176 yellow lighten-2
#ffd54f amber lighten-2
#ffb74d orange lighten-2
#ff8a65 deep-orange lighten-2
#a1887f brown lighten-2
#e0e0e0 grey lighten-2
#90a4ae blue-grey lighten-2

#e57373  
#ba68c8
#9575cd 
#7986cb
#64b5f6 
#4fc3f7
#4dd0e1 
#4db6ac 
#81c784
#aed581 
#dce775 
#fff176
#ffd54f
#ffb74d
#ff8a65 
#a1887f
#e0e0e0 
#90a4ae


## Developing

npm run dev

## Building a dist

npm run build


